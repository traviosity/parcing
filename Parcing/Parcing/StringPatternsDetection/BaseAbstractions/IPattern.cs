﻿using System;
using System.Collections.Generic;
using System.Text;
using SystemExtensions.Math;

namespace Parcing.StringPatternsDetection
{
    /// <summary> 
    /// Provides base logic for string pattern. 
    /// </summary>
    public abstract class IPattern
    {
        private Action<PatternError> OnError { get; set; }

        /// <summary>
        /// The number of symbols at the end of the pattern. Indicates that this symbols can be used in the next pattern.
        /// </summary>
        public int DiscardedSymbolsNumber { get; protected set; }

        /// <summary>
        /// Indicates that pattern detected string
        /// </summary>
        public bool Detected { get; protected set; }

        protected PatternData Data { get; set; }

        /// <summary>
        /// The length of processed string
        /// </summary>
        public int Length => _PatternLength.Value;
        private readonly Integrator _PatternLength;

        /// <summary>
        /// The length without discarded symbols
        /// </summary>
        public int UsedLength => Length - DiscardedSymbolsNumber;

        public IPattern()
        {
            Data = new PatternData();
            _PatternLength = new Integrator();
        }

        /// <summary>
        /// Processes next symbol. Result is 'true' if symbol matches the pattern, otherwise 'false'.
        /// </summary>
        public bool CheckNextSymbol(char symbol)
        {
            bool result = SymbolProcessing(symbol);
            if (result)
                _PatternLength.Integrate();

            return result;
        }

        /// <summary>
        /// Informs pattern that previous symbol was the last one. Pattern analyse current state in case it can finish detection process and return something.
        /// </summary>
        public void OnSymbolsEnd()
        {
            SymbolsEndProcessing();
        }

        public virtual void Reset()
        {
            Detected = false;
            DiscardedSymbolsNumber = 0;
            Data = new PatternData();
            _PatternLength.Reset();
        }

        /// <summary>
        /// Returns detected data. Data position transforms accordingly to new starting point.
        /// </summary>
        /// <param name="offset">Start position of this pattern in current context</param>
        public PatternData GetData(int offset)
        {
            if (!Detected)
                throw new Exception("Data is not valid");

            for (int i = 0; i < Data.Count; i++)
            {
                Data[i].MoveOriginToOffset();
                Data[i].Offset = offset;
            }

            return Data;
        }

        public void AddErrorsListener(Action<PatternError> listener)
        {
            OnError += listener;
        }

        protected void ThrowError(int localStartPos, int length, string msg)
        {
            ThrowError(new PatternError(localStartPos, length, ToString(), msg));
        }

        protected void ThrowError(PatternError patternError)
        {
            patternError.PatternsCallStack.Insert(0, GetType());
            OnError.Invoke(patternError);
        }

        /// <summary>
        /// Processes next symbol. Result is 'true' if symbol matches the pattern, otherwise 'false'.
        /// </summary>
        protected abstract bool SymbolProcessing(char symbol);

        /// <summary>
        /// Informs pattern that previous symbol was the last one. Pattern analyse current state in case it can finish detection process and return something.
        /// </summary>
        protected abstract void SymbolsEndProcessing();

        public static string GetPatternsString(IEnumerable<IPattern> patterns)
        {
            StringBuilder result = new StringBuilder();
            foreach (IPattern pattern in patterns)
                result.Append(", { " + pattern.ToString() + " }");

            result.Remove(0, 2);

            return result.ToString();
        }
    }
}
