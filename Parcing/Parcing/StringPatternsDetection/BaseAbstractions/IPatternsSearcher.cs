﻿using System;
using System.Collections.Generic;
using SystemExtensions.Math;
using SystemExtensions.ObjectsManagement;

namespace Parcing.StringPatternsDetection
{
    /// <summary>
    /// Provides base logic for patterns searchers.
    /// </summary>
    public abstract class IPatternsSearcher
    {
        /// <summary>
        /// Wraps information about pattern
        /// </summary>
        protected class PatternInfo
        {
            public readonly Func<IPattern> Creator;
            public readonly IPattern Pattern;
            public int Offset;

            public PatternInfo(Func<IPattern> creator, IPattern pattern, int offset)
            {
                Creator = creator;
                Pattern = pattern;
                Offset = offset;
            }

            public PatternDataInfo GetDataInfo()
            {
                return new PatternDataInfo(Creator, Pattern.GetData(Offset));
            }
        }

        private Action<PatternErrorInfo> OnPatternError;

        protected readonly LinkedList<Func<IPattern>> PatternsCreators;
        private readonly ObjectsPull<Func<IPattern>, PatternInfo> PatternsPull;

        public int ProcessedSymbolsCount => _ProcessedSymbolsCount.Value;
        private readonly Integrator _ProcessedSymbolsCount;

        private readonly Queue<PatternDataInfo> DetectedPatterns;
        public bool PatternsDetected => DetectedPatterns.Count != 0;

        protected IPatternsSearcher()
        {
            PatternsCreators = new LinkedList<Func<IPattern>>();
            PatternsPull = new ObjectsPull<Func<IPattern>, PatternInfo>();

            _ProcessedSymbolsCount = new Integrator();

            DetectedPatterns = new Queue<PatternDataInfo>();
        }

        public void AddPattern(Func<IPattern> patternCreator)
        {
            if (PatternsCreators.Contains(patternCreator))
                throw new Exception("Error on adding new pattern creator. Pattern creator already exists.");

            PatternsCreators.AddLast(patternCreator);
        }

        public void AddPatternsErrorsListener(Action<PatternErrorInfo> listener)
        {
            OnPatternError += listener;
        }

        /// <summary>
        /// Process next symbol.
        /// </summary>
        public void CheckNextSymbol(char symbol)
        {
            SymbolProcessing(symbol);
            _ProcessedSymbolsCount.Integrate();
        }

        /// <summary>
        /// Process symbols end.
        /// </summary>
        public void OnSymbolsEnd()
        {
            SymbolsEndProcessing();
        }

        public PatternDataInfo PullNextDetectedPattern()
        {
            return DetectedPatterns.Dequeue();
        }

        public PatternDataInfo PeekNextDetectedPattern()
        {
            return DetectedPatterns.Peek();
        }

        public virtual void Reset()
        {
            DetectedPatterns.Clear();
            _ProcessedSymbolsCount.Reset();
        }

        protected PatternInfo LoadPattern(Func<IPattern> creator, int offset)
        {
            if (PatternsPull.TryPull(creator, out PatternInfo result))
            {
                result.Pattern.Reset();
                result.Offset = offset;
            }
            else
            {
                result = new PatternInfo(creator, creator(), offset);
                result.Pattern.AddErrorsListener((PatternError patternError) =>
                {
                    patternError.Offset = result.Offset;
                    ThrowError(new PatternErrorInfo(creator, patternError));
                });
            }

            return result;
        }

        private void ThrowError(PatternErrorInfo errorInfo)
        {
            if (OnPatternError != null)
                OnPatternError.Invoke(errorInfo);
            else
                throw new Exception(string.Format("[{0}..{1}] Error in pattern \"{2}\": {3}. Patterns call stack: {4}",
                                                   errorInfo._PatternError.LocalStartPos,
                                                   errorInfo._PatternError.LocalEndPos,
                                                   errorInfo._PatternError.PatternString,
                                                   errorInfo._PatternError.Message,
                                                   errorInfo._PatternError.GetCallStackString()));
        }

        protected void SavePattern(PatternInfo patternInfo)
        {
            PatternsPull.Push(patternInfo.Creator, patternInfo);
        }

        protected void PushDetectedPatternData(PatternInfo patternInfo)
        {
            PushDetectedPatternData(patternInfo.GetDataInfo());
        }

        protected void PushDetectedPatternData(PatternDataInfo patternDataInfo)
        {
            DetectedPatterns.Enqueue(patternDataInfo);
        }

        /// <summary>Implements concrete logic of processing new symbol by patterns.</summary>
        protected abstract void SymbolProcessing(char symbol);
        /// <summary>Implements concrete logic of processing symbols end by patterns.</summary>
        protected abstract void SymbolsEndProcessing();
    }
}
