﻿using System;

namespace Parcing.StringPatternsDetection.Patterns.PrivateLogic
{
    public class AbortError
    {
        public Func<PatternData, string> MessageBuilder;
        public Func<PatternData, int> ErrorPosCalculator;
        public Func<PatternData, int> ErrorLengthCalculator;

        public AbortError(Func<PatternData, string> messageBuilder, Func<PatternData, int> errorPosCalculator = null, Func<PatternData, int> errorLengthCalculator = null)
        {
            MessageBuilder = messageBuilder;
            ErrorPosCalculator = errorPosCalculator;
            ErrorLengthCalculator = errorLengthCalculator;
        }
    }
}
