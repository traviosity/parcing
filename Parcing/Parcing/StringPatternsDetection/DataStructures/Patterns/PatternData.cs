﻿using System;
using System.Collections.Generic;

namespace Parcing.StringPatternsDetection
{
    /// <summary> 
    /// Stores information about string that was detected by some pattern. String is divided into atoms by pattern. 
    /// </summary>
    public class PatternData : List<PatternDataAtom>
    {
        public PatternData() : base()
        { }

        public PatternData(params PatternDataAtom[] atoms) : base()
        {
            for (int i = 0; i < atoms.Length; i++)
                Add(atoms[i]);
        }

        public void MergeAll()
        {
            while (Count != 1)
            {
                this[0] = PatternDataAtom.Merge(this[0], this[1]);
                RemoveAt(1);
            }
        }
    }

    /// <summary> 
    /// Stores piece of information about string that was detected by pattern. 
    /// </summary>
    public class PatternDataAtom
    {
        /// <summary>A constant used to shift coordinates.</summary>
        public int Offset;
        /// <summary>Start position in subpattern frame of reference.</summary>
        private int PrevLocalStartPos;
        /// <summary>End position in subpattern frame of reference.</summary>
        private int PrevLocalEndPos => PrevLocalStartPos + Length - 1;
        /// <summary>Start position in the current frame of reference.</summary>
        public int LocalStartPos => Offset + PrevLocalStartPos;
        /// <summary>Start position in the current frame of reference.</summary>
        public int LocalEndPos => Offset + PrevLocalEndPos;

        /// <summary>Data length.</summary>
        public int Length => Content.Length;

        public readonly string Content;

        public PatternDataAtom(int localStartPos, string data)
        {
            if (data.Length == 0)
                throw new Exception("Data cannot be empty string");

            PrevLocalStartPos = localStartPos;
            Content = data;
        }

        public PatternDataAtom(int localStartPos, char data)
        {
            PrevLocalStartPos = localStartPos;
            Content = new string(data, 1);
        }

        /// <summary>
        /// Coordinate transformation. Global coordinates become local.
        /// </summary>
        public void MoveOriginToOffset()
        {
            PrevLocalStartPos += Offset;
            Offset = 0;
        }

        public static PatternDataAtom Merge(PatternDataAtom left, PatternDataAtom right)
        {
            if (left.LocalEndPos + 1 != right.LocalStartPos || left.Offset != right.Offset)
                throw new Exception(string.Format("Can't merge atoms \"{0}\" and \"{1}\"", left.Content, right.Content));

            return new PatternDataAtom(left.PrevLocalStartPos, left.Content + right.Content)
            {
                Offset = left.Offset
            };
        }
    }
}
