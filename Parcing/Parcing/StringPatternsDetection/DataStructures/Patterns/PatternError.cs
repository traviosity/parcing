﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parcing.StringPatternsDetection
{
    /// <summary> 
    /// Stores information about an error that occurred in some pattern. 
    /// </summary>
    public class PatternError
    {
        /// <summary>A constant used to shift coordinates.</summary>
        public int Offset;
        /// <summary>Start position in subpattern frame of reference.</summary>
        private int PrevLocalStartPos;
        /// <summary>End position in subpattern frame of reference.</summary>
        private int PrevLocalEndPos => PrevLocalStartPos + Length - 1;
        /// <summary>Start position in the current frame of reference.</summary>
        public int LocalStartPos => Offset + PrevLocalStartPos;
        /// <summary>Start position in the current frame of reference.</summary>
        public int LocalEndPos => Offset + PrevLocalEndPos;

        /// <summary>Error sector length.</summary>
        public readonly int Length;

        /// <summary>Origin pattern ToString result.</summary>
        public readonly string PatternString;
        public readonly string Message;

        /// <summary>Stack of pattern calls. Start with the top one.</summary>
        public readonly List<Type> PatternsCallStack;

        public PatternError(int localStartPos, int length, string patternString, string message)
        {
            if (length < 1)
                throw new Exception("Length cannot be less than one");

            PatternsCallStack = new List<Type>();
            PatternString = patternString;
            Message = message;
            PrevLocalStartPos = localStartPos;
            Length = length;
        }

        /// <summary>
        /// Coordinate transformation. Global coordinates become local.
        /// </summary>
        public void MoveOriginToOffset()
        {
            PrevLocalStartPos += Offset;
            Offset = 0;
        }

        public string GetCallStackString()
        {
            StringBuilder result = new StringBuilder();
            foreach (Type type in PatternsCallStack)
                result.Append("->" + type.Name);

            result.Remove(0, 2);

            return result.ToString();
        }
    }
}
