﻿using System;

namespace Parcing.StringPatternsDetection
{
    /// <summary>
    /// Wraps information about detected data
    /// </summary>
    public class PatternDataInfo
    {
        public readonly Func<IPattern> Creator;
        public readonly PatternData _PatternData;

        public PatternDataInfo(Func<IPattern> creator, PatternData patternData)
        {
            Creator = creator;
            _PatternData = patternData;
        }
    }

    /// <summary>
    /// Wraps information about pattern error
    /// </summary>
    public class PatternErrorInfo
    {
        public readonly Func<IPattern> Creator;
        public readonly PatternError _PatternError;

        public PatternErrorInfo(Func<IPattern> creator, PatternError patternError)
        {
            Creator = creator;
            _PatternError = patternError;
        }
    }
}
