﻿namespace Parcing.StringPatternsDetection
{
    /// <summary>
    /// Wraps IPattern and provides a higher level way to create custom patterns.
    /// </summary>
    public abstract class IPatternFacade : IPattern
    {
        private IPattern MainPattern;

        protected override bool SymbolProcessing(char symbol)
        {
            if (MainPattern == null)
            {
                MainPattern = CreatePattern();
                MainPattern.AddErrorsListener(ThrowError);
            }

            bool result = false;

            if (MainPattern.CheckNextSymbol(symbol))
            {

                if (MainPattern.Detected)
                    result = OnDetection();
                else
                    result = true;
            }

            return result;
        }

        protected override void SymbolsEndProcessing()
        {
            if (MainPattern == null)
            {
                MainPattern = CreatePattern();
                MainPattern.AddErrorsListener(ThrowError);
            }

            MainPattern.OnSymbolsEnd();

            if (MainPattern.Detected)
                OnDetection();
        }

        private bool OnDetection()
        {
            PatternData data = MainPattern.GetData(0);
            if (BeforeDetection(data))
            {
                Data.AddRange(data);
                DiscardedSymbolsNumber = MainPattern.DiscardedSymbolsNumber;

                Detected = true;

                return true;
            }
            else
                return false;
        }

        public override void Reset()
        {
            base.Reset();
            MainPattern?.Reset();
        }

        /// <summary>Creates pattern that will be processed by this class.</summary>
        protected abstract IPattern CreatePattern();

        /// <summary>Called before detection. If result is 'true' detection is not altered. Otherwise detection aborted. Data can be changed inside this function.</summary>
        protected virtual bool BeforeDetection(PatternData data) => true;
    }
}
