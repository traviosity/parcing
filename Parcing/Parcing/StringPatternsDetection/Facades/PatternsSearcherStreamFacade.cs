﻿using System;
using System.IO;

namespace Parcing.StringPatternsDetection
{
    public class PatternsSearcherStreamFacade : IDisposable
    {
        private readonly IPatternsSearcher PatternsSearcher;
        private readonly StreamReader SymbolsInput;

        public bool EndOfStream { get; private set; }

        public PatternsSearcherStreamFacade(IPatternsSearcher patternsSearcher, StreamReader symbolsInput)
        {
            PatternsSearcher = patternsSearcher;
            SymbolsInput = symbolsInput;

            EndOfStreamSync();
        }

        public PatternDataInfo ReadPattern()
        {
            PatternDataInfo result = PatternsSearcher.PullNextDetectedPattern();
            EndOfStreamSync();
            return result;
        }

        private void EndOfStreamSync()
        {
            if (!SymbolsInput.EndOfStream)
            {
                while (!PatternsSearcher.PatternsDetected && !SymbolsInput.EndOfStream)
                    PatternsSearcher.CheckNextSymbol((char)SymbolsInput.Read());

                if (SymbolsInput.EndOfStream)
                    PatternsSearcher.OnSymbolsEnd();
            }

            EndOfStream = !PatternsSearcher.PatternsDetected;
        }

        public void Dispose()
        {
            SymbolsInput.Dispose();
        }
    }
}
