﻿using System;
using System.Collections.Generic;

namespace Parcing.StringPatternsDetection
{
    public class PatternsSearcherTriggerFacade
    {
        private readonly IPatternsSearcher PatternsSearcher;
        private readonly Dictionary<Func<IPattern>, Action<PatternData>> Triggers;

        public PatternsSearcherTriggerFacade(IPatternsSearcher patternsSearcher)
        {
            PatternsSearcher = patternsSearcher;

            Triggers = new Dictionary<Func<IPattern>, Action<PatternData>>();
        }

        public void AddTrigger(Func<IPattern> trigger, Action<PatternData> reaction)
        {
            PatternsSearcher.AddPattern(trigger);
            Triggers.Add(trigger, reaction);
        }

        public void CheckNextSymbol(char symbol)
        {
            PatternsSearcher.CheckNextSymbol(symbol);
            CallTriggers();
        }

        public void OnSymbolsEnd()
        {
            PatternsSearcher.OnSymbolsEnd();
            CallTriggers();
        }

        private void CallTriggers()
        {
            while (PatternsSearcher.PatternsDetected)
            {
                var dataInfo = PatternsSearcher.PullNextDetectedPattern();

                if (Triggers.TryGetValue(dataInfo.Creator, out Action<PatternData> reaction))
                    reaction.Invoke(dataInfo._PatternData);
            }
        }
    }
}
