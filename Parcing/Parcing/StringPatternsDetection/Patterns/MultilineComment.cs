﻿namespace Parcing.StringPatternsDetection.Patterns
{
    public class MultilineComment : IPatternFacade
    {
        private readonly string StartTrigger;
        private readonly string EndTrigger;

        public MultilineComment(string startTrigger, string endTrigger)
        {
            StartTrigger = startTrigger;
            EndTrigger = endTrigger;
        }

        protected override IPattern CreatePattern()
        {
            return new Sequence()
                       .Add(new StringTrigger(StartTrigger))
                       .Add(new Repeat()
                                .Set(new SkipTo()
                                         .Set(new NewLine())))
                       .Add(new SkipTo()
                                .Set(new StringTrigger(EndTrigger)))
                       .AddAbortError(2, data => string.Format("End of file found, \"{0}\" expected", EndTrigger),
                                                data => 0, data => data[0].Content.Length);
        }
    }
}
