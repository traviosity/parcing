﻿using System;

namespace Parcing.StringPatternsDetection.Patterns
{
    public class NewLine : IPatternFacade
    {
        private static readonly string UNIX = "\n";
        private static readonly string UNIX_Output = "\\n";
        private static readonly string NotUNIX = "\r\n";
        private static readonly string NotUNIX_Output = "\\r\\n";
        private static readonly string SystemType = Environment.NewLine.Equals(NotUNIX) ? "non UNIX" : "UNIX";
        private static readonly string OutputType = Environment.NewLine.Equals(NotUNIX) ? NotUNIX_Output : UNIX_Output;

        protected override IPattern CreatePattern()
        {
            return new Branching().Add(new StringTrigger(UNIX))
                                  .Add(new StringTrigger(NotUNIX));
        }

        protected override bool BeforeDetection(PatternData data)
        {
            if (!Environment.NewLine.Equals(data[0].Content))
            {
                ThrowError(0, data[0].Content.Length, string.Format("Expected {0} ({1}) new line string", SystemType, OutputType));
                return false;
            }
            else
                return true;
        }
    }
}
