﻿namespace Parcing.StringPatternsDetection.Patterns
{
    public class OneLineComment : IPatternFacade
    {
        private readonly string StartTrigger;

        public OneLineComment(string startTrigger)
        {
            StartTrigger = startTrigger;
        }

        protected override IPattern CreatePattern() => new Sequence()
                                                           .Add(new StringTrigger(StartTrigger))
                                                           .Add(new SkipTo()
                                                                    .Set(new NewLine())
                                                                    .ExcludeEndPatternFromResult());
    }
}
