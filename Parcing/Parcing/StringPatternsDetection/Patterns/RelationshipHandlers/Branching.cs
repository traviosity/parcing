﻿using System;
using System.Collections.Generic;

namespace Parcing.StringPatternsDetection.Patterns
{
    /// <summary>
    /// Detects one of the patterns.
    /// </summary>
    public class Branching : IPattern
    {
        private bool CanSkip;

        private readonly List<IPattern> PatternsBranches;

        private readonly LinkedList<IPattern> ActivePatterns;
        private bool InsideAnyPattern => ActivePatterns.Count != 0;

        private IPattern DetectedPattern;
        private int SymbolsNumAfterDetectedPattern;

        /// <summary>
        /// By default cannot be skiped
        /// </summary>
        public Branching()
        {
            PatternsBranches = new List<IPattern>();
            ActivePatterns = new LinkedList<IPattern>();
        }

        /// <summary>
        /// Adds another branch.
        /// </summary>
        /// <returns>Object of this branching class</returns>
        public Branching Add(IPattern pattern)
        {
            pattern.AddErrorsListener(ThrowError);
            PatternsBranches.Add(pattern);

            return this;
        }

        /// <summary>
        /// If skip enabled, branching pattern will be detected, even if all branches will aborts.
        /// </summary>
        /// <returns>Object of this branching class</returns>
        public Branching EnableSkip()
        {
            CanSkip = true;

            return this;
        }

        protected override bool SymbolProcessing(char symbol)
        {
            if (PatternsBranches.Count < 1)
                throw new Exception("Must contain at least one pattern branch.");

            bool result = false;

            bool firstPatternIter = false;
            if (!InsideAnyPattern)
            {
                foreach (IPattern pattern in PatternsBranches)
                    if (pattern.CheckNextSymbol(symbol))
                        ActivePatterns.AddLast(pattern);

                if (InsideAnyPattern) // entered any pattern
                    firstPatternIter = true;
            }

            if (InsideAnyPattern)
            {
                LinkedList<IPattern> finishedPatterns = new LinkedList<IPattern>();
                LinkedList<IPattern> abortedPatterns = new LinkedList<IPattern>();

                foreach (IPattern pattern in ActivePatterns)
                {
                    if (firstPatternIter || pattern.CheckNextSymbol(symbol))
                    {
                        if (pattern.Detected)
                            finishedPatterns.AddLast(pattern);
                    }
                    else
                        abortedPatterns.AddLast(pattern);
                }

                foreach (IPattern pattern in abortedPatterns) // remove aborted patterns
                    ActivePatterns.Remove(pattern);

                if (DetectedPattern != null)
                    SymbolsNumAfterDetectedPattern++;

                if (InsideAnyPattern)
                {
                    result = true;

                    if (finishedPatterns.Count > 0)
                    {
                        foreach (IPattern pattern in finishedPatterns)
                            ActivePatterns.Remove(pattern);

                        var maxFinishedPatterns = FindMaxUsedLengthPattern(finishedPatterns);

                        if (maxFinishedPatterns.Count > 1)
                            throw new Exception("Patterns { " + GetPatternsString(maxFinishedPatterns) + " } react on the same sequence");

                        UpdateDetectedPattern(maxFinishedPatterns.First.Value);

                        if (!InsideAnyPattern)
                            OnDetection(DetectedPattern, SymbolsNumAfterDetectedPattern);
                    }
                }
                else if (DetectedPattern != null) // all aborted
                {
                    result = true;
                    OnDetection(DetectedPattern, SymbolsNumAfterDetectedPattern);
                }
                else if (CanSkip)
                {
                    DiscardedSymbolsNumber = Length + 1;
                    Detected = true;

                    result = true;
                }
            }
            else if (CanSkip)
            {
                DiscardedSymbolsNumber = Length + 1;
                Detected = true;

                result = true;
            }

            return result;
        }

        protected override void SymbolsEndProcessing()
        {
            LinkedList<IPattern> abortedPatterns = new LinkedList<IPattern>();
            foreach (IPattern pattern in ActivePatterns)
            {
                pattern.OnSymbolsEnd();

                if (!pattern.Detected)
                    abortedPatterns.AddLast(pattern);
            }

            foreach (IPattern pattern in abortedPatterns)
                ActivePatterns.Remove(pattern);

            if (InsideAnyPattern)
            {
                var maxFinishedPatterns = FindMaxUsedLengthPattern(ActivePatterns);

                ActivePatterns.Clear();

                if (maxFinishedPatterns.Count > 1)
                    throw new Exception("Patterns { " + GetPatternsString(maxFinishedPatterns) + " } react on the same sequence");

                UpdateDetectedPattern(maxFinishedPatterns.First.Value);

                OnDetection(DetectedPattern, SymbolsNumAfterDetectedPattern);
            }
            else if (DetectedPattern != null)
                OnDetection(DetectedPattern, SymbolsNumAfterDetectedPattern);
            else if (CanSkip)
            {
                DiscardedSymbolsNumber = Length;
                Detected = true;
            }
        }

        private LinkedList<IPattern> FindMaxUsedLengthPattern(LinkedList<IPattern> patterns)
        {
            LinkedList<IPattern> result = new LinkedList<IPattern>();
            int currUsedLength = patterns.First.Value.UsedLength;
            foreach (IPattern pattern in patterns)
            {
                if (pattern.UsedLength == currUsedLength)
                    result.AddLast(pattern);
                else if (pattern.UsedLength > currUsedLength)
                {
                    result.Clear();
                    result.AddLast(pattern);
                }
            }

            return result;
        }

        private void UpdateDetectedPattern(IPattern pattern)
        {
            if (DetectedPattern == null || pattern.UsedLength > DetectedPattern.UsedLength ||
                (pattern.UsedLength == DetectedPattern.UsedLength && pattern.Length > DetectedPattern.Length))
            {
                DetectedPattern = pattern;
                SymbolsNumAfterDetectedPattern = 0;
            }
        }

        private void OnDetection(IPattern pattern, int symbolsNumAfterPattern)
        {
            Data.AddRange(pattern.GetData(0));
            DiscardedSymbolsNumber = pattern.DiscardedSymbolsNumber + symbolsNumAfterPattern;

            Detected = true;
        }

        public override void Reset()
        {
            base.Reset();

            for (int i = 0; i < PatternsBranches.Count; i++)
                PatternsBranches[i].Reset();
            ActivePatterns.Clear();
            DetectedPattern = null;
        }
    }
}
