﻿using System;

namespace Parcing.StringPatternsDetection.Patterns
{
    /// <summary>
    /// Provides ability to the create recursion patterns. For examples, see class RecursionEntryTests in ParcingTests project.
    /// </summary>
    public class RecursionEntry : IPattern
    {
        private readonly Func<IPattern> RecursionPatternCreator;
        private IPattern RecursionPattern;

        public RecursionEntry(Func<IPattern> recursionPatternCreator)
        {
            RecursionPatternCreator = recursionPatternCreator;
        }

        protected override bool SymbolProcessing(char symbol)
        {
            bool result = false;

            if (RecursionPattern == null)
                OnCreate();

            if (RecursionPattern.CheckNextSymbol(symbol))
            {
                result = true;

                if (RecursionPattern.Detected)
                    OnDetection();
            }

            return result;
        }

        protected override void SymbolsEndProcessing()
        {
            RecursionPattern.OnSymbolsEnd();

            if (RecursionPattern.Detected)
                OnDetection();
        }

        private void OnDetection()
        {
            Data.AddRange(RecursionPattern.GetData(0));
            DiscardedSymbolsNumber = RecursionPattern.DiscardedSymbolsNumber;

            Detected = true;
        }

        private void OnCreate()
        {
            RecursionPattern = RecursionPatternCreator();
            RecursionPattern.AddErrorsListener(ThrowError);
        }

        public override void Reset()
        {
            base.Reset();

            RecursionPattern = null;
        }
    }
}
