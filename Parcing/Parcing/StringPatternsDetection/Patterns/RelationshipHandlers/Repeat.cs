﻿using Parcing.StringPatternsDetection.Patterns.PrivateLogic;
using System;
using System.Collections.Generic;

namespace Parcing.StringPatternsDetection.Patterns
{
    /// <summary>
    /// Detects repetitions of a specific pattern. You can set limits on the number of repetitions.
    /// </summary>
    public class Repeat : IPattern
    {
        private IPattern PatternToRepeat;

        private int MinRepeats;
        private int MaxRepeats;

        private int CurrentRepeats;
        private int PatternStartPos;
        private LinkedList<char> SymbolsBuffer;

        private AbortError AbortError;
        private int DetectedPatternsLength;

        /// <summary>
        /// By default have minimum repeats number = 0 and maximum repeats number = int.MaxValue
        /// </summary>
        public Repeat()
        {
            MinRepeats = 0;
            MaxRepeats = int.MaxValue;
        }

        /// <summary>
        /// Sets pattern that will be repeated.
        /// </summary>
        /// <returns>Object of this repeat class</returns>
        public Repeat Set(IPattern pattern)
        {
            pattern.AddErrorsListener(patternError =>
            {
                patternError.Offset = PatternStartPos;
                patternError.MoveOriginToOffset();
                ThrowError(patternError);
            });
            PatternToRepeat = pattern;

            return this;
        }

        /// <summary>
        /// Sets minimum repeats number.
        /// </summary>
        /// <returns>Object of this repeat class</returns>
        public Repeat SetMin(int value)
        {
            if (value < 0)
                throw new Exception("The minimum number of repetitions must be zero or more.");
            if (value > MaxRepeats)
                throw new Exception("The minimum number of repetitions must be less than or equal to the maximum number of repetitions.");

            MinRepeats = value;

            return this;
        }

        /// <summary>
        /// Sets maximum repeats number.
        /// </summary>
        /// <returns>Object of this repeat class</returns>
        public Repeat SetMax(int value)
        {
            if (value < 0)
                throw new Exception("The maximum number of repetitions must be zero or more.");
            if (MinRepeats > value)
                throw new Exception("The maximum number of repetitions must be more than or equal to the minimum number of repetitions.");

            MaxRepeats = value;

            return this;
        }

        /// <summary>
        /// Sets maximum and minimum repeats number with same value.
        /// </summary>
        /// <returns>Object of this repeat class</returns>
        public Repeat SetMinMax(int value)
        {
            if (value < 0)
                throw new Exception("The maximum/minimum number of repetitions must be zero or more.");

            MinRepeats = value;
            MaxRepeats = value;

            return this;
        }

        /// <summary>
        /// Enables error in case if pattern aborts before min numbers of repeats
        /// </summary>
        /// <param name="messageBuilder">Creates error message based on all data detected so far</param>
        /// <returns>Object of this repeat class</returns>
        public Repeat EnableAbortError(Func<PatternData, string> messageBuilder)
        {
            return EnableAbortErrorBase(messageBuilder);
        }

        /// <summary>
        /// Enables error in case if pattern aborts before min numbers of repeats
        /// </summary>
        /// <param name="messageBuilder">Creates error message based on all data detected so far</param>
        /// <param name="errorPosCalculator">Returns error position based on all data detected so far</param>
        /// <param name="errorLengthCalculator">Returns error length based on all data detected so far</param>
        /// <returns>Object of this repeat class</returns>
        public Repeat EnableAbortError(Func<PatternData, string> messageBuilder,
                                    Func<PatternData, int> errorPosCalculator, Func<PatternData, int> errorLengthCalculator)
        {
            return EnableAbortErrorBase(messageBuilder, errorPosCalculator, errorLengthCalculator);
        }

        private Repeat EnableAbortErrorBase(Func<PatternData, string> messageBuilder,
                                            Func<PatternData, int> errorPosCalculator = null, Func<PatternData, int> errorLengthCalculator = null)
        {
            if (AbortError != null)
                throw new Exception(string.Format("Abort error already exists."));

            AbortError = new AbortError(messageBuilder, errorPosCalculator, errorLengthCalculator);

            return this;
        }

        protected override bool SymbolProcessing(char symbol)
        {
            if (PatternToRepeat == null)
                throw new Exception("Pattern to repeat is not set.");

            if (CurrentRepeats == MaxRepeats)
            {
                DiscardedSymbolsNumber++;
                Detected = true;
                return true;
            }
            else if (PatternToRepeat.CheckNextSymbol(symbol))
            {
                if (SymbolsBuffer == null)
                    SymbolsBuffer = new LinkedList<char>();

                SymbolsBuffer.AddLast(symbol);

                if (PatternToRepeat.Detected)
                    return OnIter(PatternToRepeat, false);
                else
                    return true;
            }
            else if (CurrentRepeats < MinRepeats)
            {
                OnAbort(false);
                return false;
            }
            else
            {
                DiscardedSymbolsNumber += SymbolsBuffer != null ? SymbolsBuffer.Count : 0;
                DiscardedSymbolsNumber++;

                Detected = true;
                return true;
            }
        }

        protected override void SymbolsEndProcessing()
        {
            if (CurrentRepeats == MaxRepeats)
            {
                Detected = true;
                return;
            }

            while (true)
            {
                PatternToRepeat.OnSymbolsEnd();

                if (PatternToRepeat.Detected)
                    OnIter(PatternToRepeat, true);
                else
                    break;

                if (Detected)
                    return;
            }

            if (CurrentRepeats < MinRepeats)
            {
                OnAbort(true);
                return;
            }
            else
            {
                DiscardedSymbolsNumber += SymbolsBuffer != null ? SymbolsBuffer.Count : 0;

                Detected = true;
            }
        }

        private bool OnIter(IPattern pattern, bool symbolsEnd)
        {
            bool result = true;

            Data.AddRange(pattern.GetData(PatternStartPos));
            DetectedPatternsLength += pattern.Length - pattern.DiscardedSymbolsNumber;

            LinkedList<char> symbolsBuffer = SymbolsBuffer;
            SymbolsBuffer = null;
            int discardedSymbolsNumber = pattern.DiscardedSymbolsNumber;
            pattern.Reset();

            DiscardedSymbolsNumber += discardedSymbolsNumber;

            CurrentRepeats++;
            PatternStartPos = Length - DiscardedSymbolsNumber + (symbolsEnd ? 0 : 1);

            if (discardedSymbolsNumber != 0)
            {
                if (discardedSymbolsNumber < 0)
                    throw new Exception("Pattern \"" + pattern + "\" generates not correct discarded symbols.");

                var node = symbolsBuffer.Last;

                for (int i = 1; i < discardedSymbolsNumber; i++)
                {
                    node = node.Previous;

                    if (node == null)
                        throw new Exception("Pattern \"" + pattern + "\" generates not correct discarded symbols.");
                }

                if (node.Previous == null)
                    throw new Exception("Detected endless recursion with pattern \"" + pattern.ToString() + "\".");

                for (int i = 0; i < discardedSymbolsNumber; i++)
                {
                    DiscardedSymbolsNumber--;

                    if (SymbolProcessing(node.Value))
                    {
                        if (Detected)
                            break;
                    }
                    else
                    {
                        result = false;
                        break;
                    }

                    node = node.Next;
                }
            }

            return result;
        }

        private void OnAbort(bool symbolsEnd)
        {
            if (AbortError != null)
            {
                int pos;
                if (AbortError.ErrorPosCalculator != null)
                    pos = AbortError.ErrorPosCalculator(Data);
                else if (symbolsEnd && DetectedPatternsLength == Length)
                    pos = DetectedPatternsLength - 1;
                else
                    pos = DetectedPatternsLength;

                int length;
                if (AbortError.ErrorLengthCalculator != null)
                    length = AbortError.ErrorLengthCalculator(Data);
                else
                    length = 1;

                ThrowError(pos, length, AbortError.MessageBuilder(Data));
            }
        }

        public override void Reset()
        {
            base.Reset();

            PatternToRepeat.Reset();

            CurrentRepeats = 0;
            PatternStartPos = 0;
            SymbolsBuffer = null;

            DetectedPatternsLength = 0;
        }
    }
}
