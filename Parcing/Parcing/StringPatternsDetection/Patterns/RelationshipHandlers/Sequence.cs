﻿using Parcing.StringPatternsDetection.Patterns.PrivateLogic;
using System;
using System.Collections.Generic;

namespace Parcing.StringPatternsDetection.Patterns
{
    /// <summary>
    /// Detects sequence of patterns.
    /// </summary>
    public class Sequence : IPattern
    {
        private int ReleasedPatternsNum;

        private readonly List<IPattern> PatternsSequence;
        private int PatternInd;
        private int PatternStartPos;

        private LinkedList<char> SymbolsBuffer;

        private int ReleasedSymbols;

        private readonly Dictionary<int, AbortError> AbortErrors;
        private int DetectedPatternsLength;

        public Sequence()
        {
            PatternsSequence = new List<IPattern>();
            AbortErrors = new Dictionary<int, AbortError>();
        }

        /// <summary>
        /// Adds pattern to the end of the sequence.
        /// </summary>
        /// <returns>Object of this sequence class</returns>
        public Sequence Add(IPattern pattern)
        {
            pattern.AddErrorsListener(patternError =>
            {
                patternError.Offset = PatternStartPos;
                patternError.MoveOriginToOffset();
                ThrowError(patternError);
            });
            PatternsSequence.Add(pattern);

            return this;
        }

        /// <summary>
        /// Sets the number of patterns at the end of sequence that will be available for the next patterns
        /// </summary>
        /// <param name="releasedPatternsNum">Number of patterns at the end of sequence</param>
        /// <returns>Object of this sequence class</returns>
        public Sequence SetReleasedPatternNumber(int releasedPatternsNum)
        {
            ReleasedPatternsNum = releasedPatternsNum;

            return this;
        }

        /// <summary>
        /// Adds error in case if sequence aborts with specific pattern
        /// </summary>
        /// <param name="patternIndex">Error will be thrown if pattern with this index aborted</param>
        /// <param name="messageBuilder">Creates error message based on all data detected so far</param>
        /// <returns>Object of this sequence class</returns>
        public Sequence AddAbortError(int patternIndex, Func<PatternData, string> messageBuilder)
        {
            return AddAbortErrorBase(patternIndex, messageBuilder);
        }

        /// <summary>
        /// Adds error in case if sequence aborts with specific pattern
        /// </summary>
        /// <param name="patternIndex">Error will be thrown if pattern with this index aborted</param>
        /// <param name="messageBuilder">Returns error message based on all data detected so far</param>
        /// <param name="errorPosCalculator">Returns error position based on all data detected so far</param>
        /// <param name="errorLengthCalculator">Returns error length based on all data detected so far</param>
        /// <returns>Object of this sequence class</returns>
        public Sequence AddAbortError(int patternIndex, Func<PatternData, string> messageBuilder,
                                      Func<PatternData, int> errorPosCalculator, Func<PatternData, int> errorLengthCalculator)
        {
            return AddAbortErrorBase(patternIndex, messageBuilder, errorPosCalculator, errorLengthCalculator);
        }

        private Sequence AddAbortErrorBase(int patternIndex, Func<PatternData, string> messageBuilder,
                                           Func<PatternData, int> errorPosCalculator = null, Func<PatternData, int> errorLengthCalculator = null)
        {
            if (AbortErrors.ContainsKey(patternIndex))
                throw new Exception(string.Format("Error for pattern with index \"{0}\" already exists.", patternIndex));

            AbortErrors.Add(patternIndex, new AbortError(messageBuilder, errorPosCalculator, errorLengthCalculator));

            return this;
        }

        protected override bool SymbolProcessing(char symbol)
        {
            if (ReleasedPatternsNum < 0 || ReleasedPatternsNum > PatternsSequence.Count)
                throw new Exception("Released pattern number must be within sequence length.");

            if (PatternsSequence.Count < 1)
                throw new Exception("Sequence must contain at least one pattern.");

            bool result;

            var pattern = PatternsSequence[PatternInd];

            if (pattern.CheckNextSymbol(symbol))
            {
                if (SymbolsBuffer == null)
                    SymbolsBuffer = new LinkedList<char>();

                SymbolsBuffer.AddLast(symbol);

                if (pattern.Detected)
                    result = OnDetection(pattern, false);
                else
                    result = true;
            }
            else
                result = false;

            if (!result)
                OnAbort(false);

            return result;
        }

        protected override void SymbolsEndProcessing()
        {
            while (true)
            {
                var pattern = PatternsSequence[PatternInd];
                pattern.OnSymbolsEnd();

                if (pattern.Detected)
                    OnDetection(pattern, true);
                else
                    break;

                if (Detected)
                    return;
            }

            OnAbort(true);
        }

        private bool OnDetection(IPattern pattern, bool symbolsEnd)
        {
            bool result = true;

            if (PatternInd < PatternsSequence.Count - ReleasedPatternsNum)
                Data.AddRange(pattern.GetData(PatternStartPos));
            else
                ReleasedSymbols += pattern.Length;

            DetectedPatternsLength += pattern.Length - pattern.DiscardedSymbolsNumber;

            LinkedList<char> symbolsBuffer = SymbolsBuffer;
            SymbolsBuffer = null;
            int discardedSymbolsNumber = pattern.DiscardedSymbolsNumber;
            pattern.Reset();

            DiscardedSymbolsNumber += discardedSymbolsNumber;

            PatternInd++;
            PatternStartPos = Length - DiscardedSymbolsNumber + (symbolsEnd ? 0 : 1);

            if (PatternInd == PatternsSequence.Count)
            {
                if (ReleasedPatternsNum != 0)
                    DiscardedSymbolsNumber = ReleasedSymbols;

                Detected = true;
            }
            else if (discardedSymbolsNumber != 0)
            {
                if (discardedSymbolsNumber < 0)
                    throw new Exception("Pattern \"" + pattern + "\" generates not correct discarded symbols");

                var node = symbolsBuffer.Last;

                for (int i = 1; i < discardedSymbolsNumber; i++)
                {
                    node = node.Previous;

                    if (node == null)
                        throw new Exception("Pattern \"" + pattern + "\" generates not correct discarded symbols");
                }

                for (int i = 0; i < discardedSymbolsNumber; i++)
                {
                    DiscardedSymbolsNumber--;

                    if (SymbolProcessing(node.Value))
                    {
                        if (Detected)
                            break;
                    }
                    else
                    {
                        result = false;
                        break;
                    }

                    node = node.Next;
                }
            }

            return result;
        }

        private void OnAbort(bool symbolsEnd)
        {
            if (AbortErrors.TryGetValue(PatternInd, out AbortError abortError))
            {
                int pos;
                if (abortError.ErrorPosCalculator != null)
                    pos = abortError.ErrorPosCalculator(Data);
                else if (symbolsEnd && DetectedPatternsLength == Length)
                    pos = DetectedPatternsLength - 1;
                else
                    pos = DetectedPatternsLength;

                int length;
                if (abortError.ErrorLengthCalculator != null)
                    length = abortError.ErrorLengthCalculator(Data);
                else
                    length = 1;

                ThrowError(pos, length, abortError.MessageBuilder(Data));
            }
        }

        public override void Reset()
        {
            base.Reset();

            SymbolsBuffer = null;
            for (int i = 0; i < PatternsSequence.Count; i++)
                PatternsSequence[i].Reset();

            PatternInd = 0;
            PatternStartPos = 0;
            ReleasedSymbols = 0;

            DetectedPatternsLength = 0;
        }
    }
}
