﻿using System.Text;

namespace Parcing.StringPatternsDetection.Patterns
{
    /// <summary>
    /// Skipes symbols until a pattern is found.
    /// </summary>
    public class SkipTo : IPattern
    {
        private bool IncludeEndPatternInResult;
        private IPattern EndPattern;

        private readonly StringBuilder SkipedSymbols;

        /// <summary>
        /// By default includes end pattern in result.
        /// </summary>
        public SkipTo()
        {
            SkipedSymbols = new StringBuilder();

            IncludeEndPatternInResult = true;
        }

        /// <summary>
        /// Sets pattern on which skipping will stop.
        /// </summary>
        /// <returns>Object of this skipTo class</returns>
        public SkipTo Set(IPattern pattern)
        {
            pattern.AddErrorsListener(ThrowError);
            EndPattern = pattern;

            return this;
        }

        /// <summary>
        /// End pattern will be available for the next pattern.
        /// </summary>
        /// <returns>Object of this skipTo class</returns>
        public SkipTo ExcludeEndPatternFromResult()
        {
            IncludeEndPatternInResult = false;

            return this;
        }

        protected override bool SymbolProcessing(char symbol)
        {
            if (EndPattern.CheckNextSymbol(symbol))
            {
                if (EndPattern.Detected)
                    OnDetection();
            }
            else
                SkipedSymbols.Append(symbol);

            return true;
        }

        protected override void SymbolsEndProcessing()
        {
            EndPattern.OnSymbolsEnd();

            if (EndPattern.Detected)
                OnDetection();
        }

        private void OnDetection()
        {
            if (SkipedSymbols.Length != 0)
                Data.Add(new PatternDataAtom(0, SkipedSymbols.ToString()));

            if (IncludeEndPatternInResult)
            {
                Data.AddRange(EndPattern.GetData(SkipedSymbols.Length));
                DiscardedSymbolsNumber = EndPattern.DiscardedSymbolsNumber;
            }
            else
                DiscardedSymbolsNumber = EndPattern.Length;

            Detected = true;
        }

        public override void Reset()
        {
            base.Reset();

            SkipedSymbols.Clear();
            EndPattern.Reset();
        }
    }
}
