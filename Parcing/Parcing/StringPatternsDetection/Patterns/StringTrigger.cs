﻿using System;
using SystemExtensions.Math;

namespace Parcing.StringPatternsDetection.Patterns
{
    /// <summary>
    /// Detects passed string
    /// </summary>
    public class StringTrigger : IPattern
    {
        private readonly Integrator Position;

        public readonly string ToDetect;

        public StringTrigger(string toDetect)
        {
            if (toDetect.Length == 0)
                throw new Exception("toDetect string length can't be zero");

            Position = new Integrator();
            ToDetect = toDetect;
        }

        protected override bool SymbolProcessing(char symbol)
        {
            if (ToDetect[Position.Value] == symbol)
            {
                if (Position.Value == ToDetect.Length - 1)
                {
                    Data.Add(new PatternDataAtom(0, ToDetect));
                    Detected = true;
                }
                else
                    Position.Integrate();

                return true;
            }
            else
                return false;
        }

        protected override void SymbolsEndProcessing()
        {
            Detected = false;
        }

        public override void Reset()
        {
            base.Reset();
            Position.Reset();
        }

        public override string ToString()
        {
            return "String trigger pattern. Trigger: \"" + ToDetect + "\"";
        }
    }
}
