﻿namespace Parcing.StringPatternsDetection.Patterns
{
    /// <summary>
    /// Detects every possible symbols
    /// </summary>
    public class AnySymbol : IPattern
    {
        protected override bool SymbolProcessing(char symbol)
        {
            Data.Add(new PatternDataAtom(Length, symbol));

            Detected = true;

            return Detected;
        }

        protected override void SymbolsEndProcessing()
        {
            Detected = false;
        }

        public override string ToString()
        {
            return "Any symbol pattern";
        }
    }
}
