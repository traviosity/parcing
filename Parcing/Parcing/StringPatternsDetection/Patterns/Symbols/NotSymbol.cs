﻿using System;
using System.Linq;

namespace Parcing.StringPatternsDetection.Patterns
{
    /// <summary>
    /// Detects symbols that weren't ignored
    /// </summary>
    public class NotSymbol : IPattern
    {
        public readonly string ToIgnore;

        public NotSymbol(string symbols)
        {
            if (symbols.Length == 0)
                throw new Exception("Symbols string length can't be zero");

            ToIgnore = symbols;
        }

        public NotSymbol(char symbol) : this(new string(symbol, 1))
        { }

        protected override bool SymbolProcessing(char symbol)
        {
            Detected = !ToIgnore.Contains(symbol);

            if (Detected)
                Data.Add(new PatternDataAtom(Length, symbol));

            return Detected;
        }

        protected override void SymbolsEndProcessing()
        {
            Detected = false;
        }

        public override string ToString()
        {
            return "Not symbol pattern. Trigger: \"" + ToIgnore + "\"";
        }
    }
}
