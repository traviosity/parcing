﻿using System;
using System.Linq;

namespace Parcing.StringPatternsDetection.Patterns
{
    /// <summary>
    /// Detects one of the passed symbols
    /// </summary>
    public class Symbol : IPattern
    {
        public readonly string ToDetect;

        public Symbol(string symbols)
        {
            if (symbols.Length == 0)
                throw new Exception("Symbols string length can't be zero");

            ToDetect = symbols;
        }

        public Symbol(char symbol) : this(new string(symbol, 1))
        { }

        protected override bool SymbolProcessing(char symbol)
        {
            Detected = ToDetect.Contains(symbol);

            if (Detected)
                Data.Add(new PatternDataAtom(Length, symbol));

            return Detected;
        }

        protected override void SymbolsEndProcessing()
        {
            Detected = false;
        }

        public override string ToString()
        {
            return "Symbol pattern. Trigger: \"" + ToDetect + "\"";
        }
    }
}
