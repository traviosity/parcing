﻿namespace Parcing.StringPatternsDetection.Patterns
{
    /// <summary>
    /// Detects a string consisting only of the given characters.
    /// </summary>
    public class SymbolsLine : IPatternFacade
    {
        private readonly string Symbols;

        public SymbolsLine(string symbols)
        {
            Symbols = symbols;
        }

        protected override IPattern CreatePattern() => new Repeat().Set(new Symbol(Symbols))
                                                                   .SetMin(1);

        protected override bool BeforeDetection(PatternData data)
        {
            data.MergeAll();
            return true;
        }
    }
}
