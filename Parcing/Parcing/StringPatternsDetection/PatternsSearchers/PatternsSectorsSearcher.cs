﻿using System;
using System.Collections.Generic;
using System.Linq;
using SystemExtensions.ClassExtensions;

namespace Parcing.StringPatternsDetection.PatternsSearchers
{
    public class PatternsIntersectionException : Exception
    {
        public PatternsIntersectionException(IEnumerable<IPattern> patterns,
                                             IEnumerable<char> symbolSequence) : base("Patterns { " + IPattern.GetPatternsString(patterns) + " } react on the same sequence \"" +
                                                                                      StringExtensions.GetStringFromChars(symbolSequence) + "\"")
        { }
    }

    /// <summary>
    /// Divide symbols stream by sectors. Sector can be detected only by one pattern. Also there is sectors that doesn't match any patterns.
    /// If sector detected inside another sector, subsector will be ignored.
    /// </summary>
    public class PatternsSectorsSearcher : IPatternsSearcher
    {
        private readonly LinkedList<PatternInfo> ActivePatterns;
        private bool InsideAnyPattern => ActivePatterns.Count != 0;

        private LinkedList<char> SymbolsBuffer;
        private int SymbolsBufferPosition;

        private PatternInfo DetectedPatternInfo;
        private LinkedListNode<char> DetectedPatternSymbolNode;

        private LinkedList<char> NoPatternBuffer;
        private int NoPatternPosition;

        public PatternsSectorsSearcher() : base()
        {
            ActivePatterns = new LinkedList<PatternInfo>();
        }

        protected override void SymbolProcessing(char symbol)
        {
            DetectionIter(symbol, ProcessedSymbolsCount);
        }

        protected override void SymbolsEndProcessing()
        {
            while (InsideAnyPattern)
            {
                LinkedList<PatternInfo> abortedPatterns = new LinkedList<PatternInfo>();
                foreach (PatternInfo patternInfo in ActivePatterns)
                {
                    patternInfo.Pattern.OnSymbolsEnd();

                    if (!patternInfo.Pattern.Detected)
                        abortedPatterns.AddLast(patternInfo);
                }

                foreach (PatternInfo patternInfo in abortedPatterns)
                {
                    ActivePatterns.Remove(patternInfo);
                    SavePattern(patternInfo);
                }

                if (!InsideAnyPattern) // all aborted
                {
                    if (DetectedPatternInfo == null)
                        IncrementSectorStart();
                    else
                        ApplyTopDetectedPattern();
                }
                else
                {
                    LinkedList<PatternInfo> maxFinishedPatterns = FindMaxUsedLengthPatternInfo(ActivePatterns);

                    if (maxFinishedPatterns.Count > 1)
                        throw new Exception("Patterns { " + IPattern.GetPatternsString(maxFinishedPatterns.Select(x => x.Pattern)) + " } react on the same sequence");

                    foreach (PatternInfo info in ActivePatterns)
                        if (maxFinishedPatterns.First.Value != info)
                            SavePattern(info);

                    ActivePatterns.Clear();

                    UpdateDetectedPatternInfo(maxFinishedPatterns.First.Value, SymbolsBuffer.Last);

                    ApplyTopDetectedPattern();
                }
            }

            WriteNoPatternBuffer();
        }

        public override void Reset()
        {
            base.Reset();

            ActivePatterns.Clear();
            NoPatternBuffer = null;
        }

        private void DetectionIter(char symbol, int position)
        {
            bool firstSectorSymbol = false;

            if (!InsideAnyPattern)
            {
                foreach (var creator in PatternsCreators)
                {
                    PatternInfo patternInfo = LoadPattern(creator, position);
                    if (patternInfo.Pattern.CheckNextSymbol(symbol))
                        ActivePatterns.AddLast(patternInfo);
                    else
                        SavePattern(patternInfo);
                }

                if (InsideAnyPattern) // entered any pattern
                {
                    SymbolsBuffer = new LinkedList<char>();
                    SymbolsBufferPosition = position;
                    DetectedPatternInfo = null;
                    DetectedPatternSymbolNode = null;

                    firstSectorSymbol = true;
                }
                else
                    AddNoPatternSymbol(symbol, position);
            }

            if (InsideAnyPattern)
            {
                LinkedList<PatternInfo> finishedPatterns = new LinkedList<PatternInfo>();
                LinkedList<PatternInfo> abortedPatterns = new LinkedList<PatternInfo>();

                foreach (PatternInfo patternInfo in ActivePatterns)
                {
                    if (firstSectorSymbol || patternInfo.Pattern.CheckNextSymbol(symbol))
                    {
                        if (patternInfo.Pattern.Detected)
                            finishedPatterns.AddLast(patternInfo);
                    }
                    else
                        abortedPatterns.AddLast(patternInfo);
                }

                foreach (PatternInfo patternInfo in abortedPatterns) // remove aborted patterns
                {
                    ActivePatterns.Remove(patternInfo);
                    SavePattern(patternInfo);
                }
                abortedPatterns.Clear();

                if (InsideAnyPattern)
                {
                    SymbolsBuffer.AddLast(symbol);

                    if (finishedPatterns.Count > 0)
                    {
                        foreach (PatternInfo info in finishedPatterns)
                            ActivePatterns.Remove(info);

                        LinkedList<PatternInfo> maxFinishedPatterns = FindMaxUsedLengthPatternInfo(finishedPatterns);

                        if (maxFinishedPatterns.Count > 1)
                            throw new Exception("Patterns { " + IPattern.GetPatternsString(maxFinishedPatterns.Select(x => x.Pattern)) + " } react on the same sequence");

                        foreach (PatternInfo info in finishedPatterns)
                            if (maxFinishedPatterns.First.Value != info)
                                SavePattern(info);

                        UpdateDetectedPatternInfo(maxFinishedPatterns.First.Value, SymbolsBuffer.Last);

                        if (!InsideAnyPattern) // last pattern finished
                            ApplyTopDetectedPattern();
                    }
                }
                else // all patterns aborted
                {
                    if (DetectedPatternInfo == null)
                        IncrementSectorStart();
                    else
                        ApplyTopDetectedPattern();

                    DetectionIter(symbol, position);
                }
            }
        }

        private LinkedList<PatternInfo> FindMaxUsedLengthPatternInfo(LinkedList<PatternInfo> patternInfos)
        {
            LinkedList<PatternInfo> result = new LinkedList<PatternInfo>();
            int currUsedLength = patternInfos.First.Value.Pattern.UsedLength;
            foreach (PatternInfo info in patternInfos)
            {
                if (info.Pattern.UsedLength == currUsedLength)
                    result.AddLast(info);
                else if (info.Pattern.UsedLength > currUsedLength)
                {
                    result.Clear();
                    result.AddLast(info);
                }
            }

            return result;
        }

        private void UpdateDetectedPatternInfo(PatternInfo patternInfo, LinkedListNode<char> lastNode)
        {
            if (DetectedPatternInfo == null || patternInfo.Pattern.UsedLength > DetectedPatternInfo.Pattern.UsedLength ||
                (patternInfo.Pattern.UsedLength == DetectedPatternInfo.Pattern.UsedLength && patternInfo.Pattern.Length > DetectedPatternInfo.Pattern.Length))
            {
                if (DetectedPatternInfo != null)
                    SavePattern(DetectedPatternInfo);

                DetectedPatternInfo = patternInfo;
                DetectedPatternSymbolNode = lastNode;
            }
            else
                SavePattern(patternInfo);
        }

        private void ApplyTopDetectedPattern()
        {
            var node = DetectedPatternSymbolNode;
            var patternInfo = DetectedPatternInfo;

            if (DetectedPatternInfo.Pattern.UsedLength != 0)
            {
                WriteDetectedPattern(patternInfo, node);

                node = node.Next;
                int i = 0;
                while (node != null)
                {
                    DetectionIter(node.Value, patternInfo.Offset + patternInfo.Pattern.Length + i);
                    node = node.Next;
                    i++;
                }

                SavePattern(patternInfo);
            }
            else
            {
                SavePattern(patternInfo);
                IncrementSectorStart();
            }
        }

        private void WriteDetectedPattern(PatternInfo patternInfo, LinkedListNode<char> lastPatternSymbolNode)
        {
            WriteNoPatternBuffer();

            PushDetectedPatternData(patternInfo);
            int discardedSymbolsNumber = patternInfo.Pattern.DiscardedSymbolsNumber;

            if (discardedSymbolsNumber != 0)
            {
                if (discardedSymbolsNumber < 0)
                    throw new Exception("Pattern \"" + patternInfo.Pattern + "\" generates not correct discarded symbols");

                int pos = patternInfo.Offset + patternInfo.Pattern.Length - 1;
                for (int i = 1; i < discardedSymbolsNumber; i++)
                {
                    lastPatternSymbolNode = lastPatternSymbolNode.Previous;
                    pos--;

                    if (lastPatternSymbolNode == null)
                        throw new Exception("Pattern \"" + patternInfo.Pattern + "\" generates not correct discarded symbols");
                }

                if (lastPatternSymbolNode.Previous == null)
                    throw new Exception("Detected endless recursion with pattern \"" + patternInfo.Pattern + "\"");

                for (int i = 0; i < discardedSymbolsNumber; i++)
                {
                    DetectionIter(lastPatternSymbolNode.Value, pos);

                    lastPatternSymbolNode = lastPatternSymbolNode.Next;
                    pos++;
                }
            }
        }

        private void IncrementSectorStart()
        {
            AddNoPatternSymbol(SymbolsBuffer.First.Value, SymbolsBufferPosition);
            SymbolsBuffer.RemoveFirst();
            SymbolsBufferPosition++;

            var symbols = SymbolsBuffer;
            int pos = SymbolsBufferPosition;
            foreach (char symbol in symbols)
            {
                DetectionIter(symbol, pos);
                pos++;
            }
        }

        private void AddNoPatternSymbol(char symbol, int position)
        {
            if (NoPatternBuffer == null)
            {
                NoPatternBuffer = new LinkedList<char>();
                NoPatternPosition = position;
            }

            NoPatternBuffer.AddLast(symbol);
        }

        private void WriteNoPatternBuffer()
        {
            if (NoPatternBuffer != null)
            {
                PushDetectedPatternData(new PatternDataInfo(null,
                                                            new PatternData(new PatternDataAtom(NoPatternPosition,
                                                                                                StringExtensions.GetStringFromChars(NoPatternBuffer)))));
                NoPatternBuffer = null;
            }
        }
    }
}
