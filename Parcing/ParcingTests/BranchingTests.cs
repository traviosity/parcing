﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Parcing.StringPatternsDetection;
using Parcing.StringPatternsDetection.Patterns;
using Parcing.StringPatternsDetection.PatternsSearchers;
using System;
using System.Collections.Generic;

namespace ParcingTests
{
    [TestClass]
    public class BranchingTests
    {
        [TestMethod]
        public void Symbols()
        {
            PatternsSearcher searcher = new PatternsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Branching().Add(new Symbol('a'))
                                                 .Add(new Symbol('b'))
                                                 .Add(new Symbol('c')); }
            };

            string testLine = "a..b...c";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 3);

            Assert.AreEqual(patternsInfo[0].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("a"));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 0);
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalEndPos, 0);

            Assert.AreEqual(patternsInfo[1].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[1]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[1]._PatternData[0].Content.Equals("b"));
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalStartPos, 3);
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalEndPos, 3);

            Assert.AreEqual(patternsInfo[2].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[2]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[2]._PatternData[0].Content.Equals("c"));
            Assert.AreEqual(patternsInfo[2]._PatternData[0].LocalStartPos, 7);
            Assert.AreEqual(patternsInfo[2]._PatternData[0].LocalEndPos, 7);
        }

        [TestMethod]
        public void Symbols_Skip()
        {
            PatternsSearcher searcher = new PatternsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Sequence().Add(new Symbol('.'))
                                                .Add(new Branching().Add(new Symbol('a'))
                                                                    .Add(new Symbol('b'))
                                                                    .EnableSkip())
                                                .Add(new Symbol('.')); }
            };

            string testLine = "...a..b.";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 3);

            Assert.AreEqual(patternsInfo[0].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 2);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("."));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 0);
            Assert.IsTrue(patternsInfo[0]._PatternData[1].Content.Equals("."));
            Assert.AreEqual(patternsInfo[0]._PatternData[1].LocalStartPos, 1);

            Assert.AreEqual(patternsInfo[1].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[1]._PatternData.Count, 3);
            Assert.IsTrue(patternsInfo[1]._PatternData[0].Content.Equals("."));
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalStartPos, 2);
            Assert.IsTrue(patternsInfo[1]._PatternData[1].Content.Equals("a"));
            Assert.AreEqual(patternsInfo[1]._PatternData[1].LocalStartPos, 3);
            Assert.IsTrue(patternsInfo[1]._PatternData[2].Content.Equals("."));
            Assert.AreEqual(patternsInfo[1]._PatternData[2].LocalStartPos, 4);

            Assert.AreEqual(patternsInfo[2].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[2]._PatternData.Count, 3);
            Assert.IsTrue(patternsInfo[2]._PatternData[0].Content.Equals("."));
            Assert.AreEqual(patternsInfo[2]._PatternData[0].LocalStartPos, 5);
            Assert.IsTrue(patternsInfo[2]._PatternData[1].Content.Equals("b"));
            Assert.AreEqual(patternsInfo[2]._PatternData[1].LocalStartPos, 6);
            Assert.IsTrue(patternsInfo[2]._PatternData[2].Content.Equals("."));
            Assert.AreEqual(patternsInfo[2]._PatternData[2].LocalStartPos, 7);
        }

        [TestMethod]
        public void Symbols_NotSkip()
        {
            PatternsSearcher searcher = new PatternsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Sequence().Add(new Symbol('.'))
                                                .Add(new Branching().Add(new Symbol('a'))
                                                                    .Add(new Symbol('b')))
                                                .Add(new Symbol('.')); }
            };

            string testLine = "...a..b.";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 2);

            Assert.AreEqual(patternsInfo[0].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 3);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("."));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 2);
            Assert.IsTrue(patternsInfo[0]._PatternData[1].Content.Equals("a"));
            Assert.AreEqual(patternsInfo[0]._PatternData[1].LocalStartPos, 3);
            Assert.IsTrue(patternsInfo[0]._PatternData[2].Content.Equals("."));
            Assert.AreEqual(patternsInfo[0]._PatternData[2].LocalStartPos, 4);

            Assert.AreEqual(patternsInfo[1].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[1]._PatternData.Count, 3);
            Assert.IsTrue(patternsInfo[1]._PatternData[0].Content.Equals("."));
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalStartPos, 5);
            Assert.IsTrue(patternsInfo[1]._PatternData[1].Content.Equals("b"));
            Assert.AreEqual(patternsInfo[1]._PatternData[1].LocalStartPos, 6);
            Assert.IsTrue(patternsInfo[1]._PatternData[2].Content.Equals("."));
            Assert.AreEqual(patternsInfo[1]._PatternData[2].LocalStartPos, 7);
        }

        [TestMethod]
        public void ErrorsHandling()
        {
            PatternErrorInfo errorInfo = null;
            PatternsSearcher searcher = new PatternsSearcher();
            searcher.AddPatternsErrorsListener(error => errorInfo = error);

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Branching().Add(new ErrorsTestPattern(2))
                                                 .Add(new ErrorsTestPattern(4)); }
            };

            string testLine = "....aaaaa";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(errorInfo.Creator, creators[0]);
            Assert.AreEqual(errorInfo._PatternError.LocalStartPos, 8);
            Assert.AreEqual(errorInfo._PatternError.LocalEndPos, 8);
            Assert.AreEqual(errorInfo._PatternError.Length, 1);
            Assert.IsTrue(errorInfo._PatternError.Message.Equals("Detected excess 'a' symbol"));

            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack.Count, 2);
            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack[0], typeof(Branching));
            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack[1], typeof(ErrorsTestPattern));
        }

        [TestMethod]
        public void PatternsPriorities_1()
        {
            PatternsSearcher searcher = new PatternsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Branching().Add(new StringTrigger("123"))
                                                 .Add(new StringTrigger("123123123")); }
            };

            string testLine = "123123123";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 1);

            Assert.AreEqual(patternsInfo[0].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("123123123"));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 0);
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalEndPos, 8);
        }

        [TestMethod]
        public void PatternsPriorities_2()
        {
            PatternsSectorsSearcher searcher = new PatternsSectorsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Branching()
                                      .Add(new Sequence()
                                               .Add(new StringTrigger("1"))
                                               .Add(new StringTrigger("2"))
                                               .Add(new StringTrigger("3"))
                                               .Add(new StringTrigger("4"))
                                               .SetReleasedPatternNumber(1))
                                      .Add(new Sequence()
                                               .Add(new StringTrigger("1"))
                                               .Add(new StringTrigger("2"))
                                               .Add(new StringTrigger("3"))
                                               .Add(new StringTrigger("4"))
                                               .SetReleasedPatternNumber(2))
                                      .Add(new Sequence()
                                               .Add(new StringTrigger("1"))
                                               .Add(new StringTrigger("2"))
                                               .Add(new StringTrigger("3"))
                                               .Add(new StringTrigger("4"))
                                               .SetReleasedPatternNumber(3)); }
            };

            string testLine = "1234";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 2);

            Assert.AreEqual(patternsInfo[0].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 3);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("1"));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 0);
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalEndPos, 0);
            Assert.IsTrue(patternsInfo[0]._PatternData[1].Content.Equals("2"));
            Assert.AreEqual(patternsInfo[0]._PatternData[1].LocalStartPos, 1);
            Assert.AreEqual(patternsInfo[0]._PatternData[1].LocalEndPos, 1);
            Assert.IsTrue(patternsInfo[0]._PatternData[2].Content.Equals("3"));
            Assert.AreEqual(patternsInfo[0]._PatternData[2].LocalStartPos, 2);
            Assert.AreEqual(patternsInfo[0]._PatternData[2].LocalEndPos, 2);

            Assert.AreEqual(patternsInfo[1].Creator, null);
            Assert.AreEqual(patternsInfo[1]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[1]._PatternData[0].Content.Equals("4"));
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalStartPos, 3);
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalEndPos, 3);
        }
    }
}
