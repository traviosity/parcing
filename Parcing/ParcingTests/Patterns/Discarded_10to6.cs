﻿using Parcing.StringPatternsDetection;

namespace ParcingTests
{
    public class Discarded_10to6 : IPattern
    {
        protected override bool SymbolProcessing(char symbol)
        {
            if (symbol.Equals('a'))
            {
                if (Length == 9)
                {
                    Data.Add(new PatternDataAtom(0, new string('a', 4)));
                    DiscardedSymbolsNumber = 6;
                    Detected = true;
                    return true;
                }

                return true;
            }
            else
                return false;
        }

        protected override void SymbolsEndProcessing()
        {
            Detected = false;
        }
    }
}
