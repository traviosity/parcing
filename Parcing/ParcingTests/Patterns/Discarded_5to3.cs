﻿using Parcing.StringPatternsDetection;

namespace ParcingTests
{
    public class Discarded_5to3 : IPattern
    {

        protected override bool SymbolProcessing(char symbol)
        {
            if (symbol.Equals('a'))
            {
                if (Length == 4)
                {
                    Data.Add(new PatternDataAtom(0, new string('a', 2)));
                    DiscardedSymbolsNumber = 3;
                    Detected = true;
                    return true;
                }

                return true;
            }
            else
                return false;
        }

        protected override void SymbolsEndProcessing()
        {
            Detected = false;
        }
    }
}
