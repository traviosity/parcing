﻿using Parcing.StringPatternsDetection;
using Parcing.StringPatternsDetection.Patterns;

namespace ParcingTests
{
    public class Discarded_Repeat : IPattern
    {
        private int SymbolsNum;
        private readonly StringTrigger Trigger;

        public Discarded_Repeat()
        {
            Trigger = new StringTrigger("ab");
        }

        protected override bool SymbolProcessing(char symbol)
        {
            if (Trigger.CheckNextSymbol(symbol))
            {
                if (Trigger.Detected)
                    Trigger.Reset();

                SymbolsNum++;
                return true;
            }
            else
            {
                if (SymbolsNum >= 2)
                {
                    Data.Add(new PatternDataAtom(0, "ab"));
                    DiscardedSymbolsNumber = SymbolsNum - 1;
                    Detected = true;
                    return true;
                }

                return false;
            }
        }

        protected override void SymbolsEndProcessing()
        {
            if (SymbolsNum >= 2)
            {
                Data.Add(new PatternDataAtom(0, "ab"));
                DiscardedSymbolsNumber = SymbolsNum - 2;
                Detected = true;
            }
        }

        public override void Reset()
        {
            base.Reset();
            Trigger.Reset();
            SymbolsNum = 0;
        }
    }
}
