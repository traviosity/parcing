﻿using Parcing.StringPatternsDetection;

namespace ParcingTests
{
    public class ErrorsTestPattern : IPattern
    {
        private int Count;

        private readonly int SymbolsCount;

        public ErrorsTestPattern(int symbolsCount)
        {
            SymbolsCount = symbolsCount;
        }

        protected override bool SymbolProcessing(char symbol)
        {
            if (symbol.Equals('a'))
            {
                if (Count == SymbolsCount)
                {
                    ThrowError(SymbolsCount, 1, "Detected excess 'a' symbol");
                    return false;
                }

                Count++;
                return true;
            }
            else
            {
                if (Count == SymbolsCount)
                {
                    Data.Add(new PatternDataAtom(0, new string('a', SymbolsCount)));
                    DiscardedSymbolsNumber = 1;
                    Detected = true;
                    return true;
                }

                return false;
            }
        }

        protected override void SymbolsEndProcessing()
        {
            if (Count == SymbolsCount)
            {
                Data.Add(new PatternDataAtom(0, new string('a', SymbolsCount)));
                Detected = true;
            }
            else
                Detected = false;
        }

        public override void Reset()
        {
            base.Reset();
            Count = 0;
        }
    }
}
