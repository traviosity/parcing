﻿using System.Collections.Generic;
using System.Linq;
using Parcing.StringPatternsDetection;

namespace ParcingTests
{
    /// <summary>
    /// Only for tests
    /// </summary>
    public class PatternsSearcher : IPatternsSearcher
    {
        private readonly LinkedList<PatternInfo> ActivePatterns;

        public PatternsSearcher()
        {
            ActivePatterns = new LinkedList<PatternInfo>();
        }

        protected override void SymbolProcessing(char symbol)
        {
            LinkedList<PatternInfo> newPatterns = new LinkedList<PatternInfo>();
            foreach (var creator in PatternsCreators)
            {
                if (!ActivePatterns.Select(x => x.Creator).Contains(creator))
                {
                    PatternInfo patternInfo = LoadPattern(creator, ProcessedSymbolsCount);
                    if (patternInfo.Pattern.CheckNextSymbol(symbol))
                    {
                        if (patternInfo.Pattern.Detected)
                        {
                            PushDetectedPatternData(patternInfo);
                            SavePattern(patternInfo);
                        }
                        else
                            newPatterns.AddLast(patternInfo);
                    }
                    else
                        SavePattern(patternInfo);
                }
            }

            LinkedList<PatternInfo> finishedPatterns = new LinkedList<PatternInfo>();
            LinkedList<PatternInfo> abortedPatterns = new LinkedList<PatternInfo>();

            {
                foreach (var patternInfo in ActivePatterns)
                {
                    if (patternInfo.Pattern.CheckNextSymbol(symbol))
                    {
                        if (patternInfo.Pattern.Detected)
                            finishedPatterns.AddLast(patternInfo);
                    }
                    else
                        abortedPatterns.AddLast(patternInfo);
                }

                foreach (var patternInfo in finishedPatterns)
                {
                    PushDetectedPatternData(patternInfo);
                    ActivePatterns.Remove(patternInfo);
                    SavePattern(patternInfo);
                }

                foreach (var patternInfo in abortedPatterns)
                {
                    ActivePatterns.Remove(patternInfo);
                    SavePattern(patternInfo);
                }
            }

            foreach (var patternInfo in newPatterns)
                ActivePatterns.AddLast(patternInfo);
        }

        protected override void SymbolsEndProcessing()
        {
            foreach (var patternInfo in ActivePatterns)
            {
                patternInfo.Pattern.OnSymbolsEnd();

                if (patternInfo.Pattern.Detected)
                    PushDetectedPatternData(patternInfo);

                SavePattern(patternInfo);
            }

            ActivePatterns.Clear();
        }

        public override void Reset()
        {
            base.Reset();
            ActivePatterns.Clear();
        }
    }
}
