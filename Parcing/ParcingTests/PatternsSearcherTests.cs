﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Parcing.StringPatternsDetection;
using Parcing.StringPatternsDetection.Patterns;
using Parcing.StringPatternsDetection.PatternsSearchers;
using System;
using System.Collections.Generic;

namespace ParcingTests
{
    [TestClass]
    public class PatternsSearcherTests
    {
        [TestMethod]
        public void SymbolsSearch()
        {
            PatternsSearcher searcher = new PatternsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Symbol('a'); },
                delegate { return new AnySymbol(); },
                delegate { return new NotSymbol('a'); }
            };

            string testLine = "ab";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 4);

            Assert.AreEqual(patternsInfo[0].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("a"));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 0);

            Assert.AreEqual(patternsInfo[1].Creator, creators[1]);
            Assert.AreEqual(patternsInfo[1]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[1]._PatternData[0].Content.Equals("a"));
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalStartPos, 0);

            Assert.AreEqual(patternsInfo[2].Creator, creators[1]);
            Assert.AreEqual(patternsInfo[2]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[2]._PatternData[0].Content.Equals("b"));
            Assert.AreEqual(patternsInfo[2]._PatternData[0].LocalStartPos, 1);

            Assert.AreEqual(patternsInfo[3].Creator, creators[2]);
            Assert.AreEqual(patternsInfo[3]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[3]._PatternData[0].Content.Equals("b"));
            Assert.AreEqual(patternsInfo[3]._PatternData[0].LocalStartPos, 1);
        }

        [TestMethod]
        public void SelfCollide()
        {
            PatternsSearcher searcher = new PatternsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new StringTrigger("aba"); }
            };

            string testLine = "ababa";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 1);

            Assert.AreEqual(patternsInfo[0].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("aba"));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 0);
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalEndPos, 2);
        }

        [TestMethod]
        public void CrossCollide()
        {
            PatternsSearcher searcher = new PatternsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new StringTrigger("1234"); },
                delegate { return new StringTrigger("3456"); }
            };

            string testLine = "123456";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 2);

            Assert.AreEqual(patternsInfo[0].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("1234"));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 0);

            Assert.AreEqual(patternsInfo[1].Creator, creators[1]);
            Assert.AreEqual(patternsInfo[1]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[1]._PatternData[0].Content.Equals("3456"));
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalStartPos, 2);
        }

        [TestMethod]
        public void PatternsErrorsHandling()
        {
            PatternErrorInfo errorInfo = null;
            PatternsSearcher searcher = new PatternsSearcher();
            searcher.AddPatternsErrorsListener(error => errorInfo = error);

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new ErrorsTestPattern(3); }
            };

            string testLine = "bbbaaaa";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 0);

            Assert.AreEqual(errorInfo.Creator, creators[0]);
            Assert.AreEqual(errorInfo._PatternError.LocalStartPos, 6);
            Assert.AreEqual(errorInfo._PatternError.LocalEndPos, 6);
            Assert.AreEqual(errorInfo._PatternError.Length, 1);
            Assert.IsTrue(errorInfo._PatternError.Message.Equals("Detected excess 'a' symbol"));

            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack.Count, 1);
            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack[0], typeof(ErrorsTestPattern));
        }
    }
}
