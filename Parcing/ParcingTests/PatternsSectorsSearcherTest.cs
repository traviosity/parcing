﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Parcing.StringPatternsDetection;
using Parcing.StringPatternsDetection.Patterns;
using Parcing.StringPatternsDetection.PatternsSearchers;
using System;
using System.Collections.Generic;

namespace ParcingTests
{
    [TestClass]
    public class PatternsSectorsSearcherTest
    {
        [TestMethod]
        public void Base()
        {
            PatternsSectorsSearcher searcher = new PatternsSectorsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new StringTrigger("123"); },
                delegate { return new StringTrigger("456"); },
                delegate { return new StringTrigger("789"); }
            };

            string testLine = "123456789";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 3);

            Assert.AreEqual(patternsInfo[0].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("123"));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 0);
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalEndPos, 2);

            Assert.AreEqual(patternsInfo[1].Creator, creators[1]);
            Assert.AreEqual(patternsInfo[1]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[1]._PatternData[0].Content.Equals("456"));
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalStartPos, 3);
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalEndPos, 5);

            Assert.AreEqual(patternsInfo[2].Creator, creators[2]);
            Assert.AreEqual(patternsInfo[2]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[2]._PatternData[0].Content.Equals("789"));
            Assert.AreEqual(patternsInfo[2]._PatternData[0].LocalStartPos, 6);
            Assert.AreEqual(patternsInfo[2]._PatternData[0].LocalEndPos, 8);
        }

        [TestMethod]
        public void SelfCollide()
        {
            PatternsSectorsSearcher searcher = new PatternsSectorsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new StringTrigger("aba"); }
            };

            string testLine = "ababa";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 2);

            Assert.AreEqual(patternsInfo[0].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("aba"));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 0);
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalEndPos, 2);

            Assert.AreEqual(patternsInfo[1].Creator, null);
            Assert.AreEqual(patternsInfo[1]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[1]._PatternData[0].Content.Equals("ba"));
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalStartPos, 3);
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalEndPos, 4);
        }

        [TestMethod]
        public void CrossIntersectionCollide()
        {
            PatternsSectorsSearcher searcher = new PatternsSectorsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new StringTrigger("1234"); },
                delegate { return new StringTrigger("3456"); }
            };

            string testLine = "123456";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 2);

            Assert.AreEqual(patternsInfo[0].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("1234"));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 0);
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalEndPos, 3);

            Assert.AreEqual(patternsInfo[1].Creator, null);
            Assert.AreEqual(patternsInfo[1]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[1]._PatternData[0].Content.Equals("56"));
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalStartPos, 4);
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalEndPos, 5);
        }

        [TestMethod]
        public void CrossInsideCollide()
        {
            PatternsSectorsSearcher searcher = new PatternsSectorsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new StringTrigger("1234"); },
                delegate { return new StringTrigger("23"); }
            };

            string testLine = "1234";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 1);

            Assert.AreEqual(patternsInfo[0].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("1234"));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 0);
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalEndPos, 3);
        }

        [TestMethod]
        public void NoPatternSectorInsidePatternsSectors()
        {
            PatternsSectorsSearcher searcher = new PatternsSectorsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new StringTrigger("123"); },
                delegate { return new StringTrigger("456"); }
            };

            string testLine = "123...456";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 3);

            Assert.AreEqual(patternsInfo[0].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("123"));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 0);
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalEndPos, 2);

            Assert.AreEqual(patternsInfo[1].Creator, null);
            Assert.AreEqual(patternsInfo[1]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[1]._PatternData[0].Content.Equals("..."));
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalStartPos, 3);
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalEndPos, 5);

            Assert.AreEqual(patternsInfo[2].Creator, creators[1]);
            Assert.AreEqual(patternsInfo[2]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[2]._PatternData[0].Content.Equals("456"));
            Assert.AreEqual(patternsInfo[2]._PatternData[0].LocalStartPos, 6);
            Assert.AreEqual(patternsInfo[2]._PatternData[0].LocalEndPos, 8);
        }

        [TestMethod]
        public void NoPatternSectorOnStart()
        {
            PatternsSectorsSearcher searcher = new PatternsSectorsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new StringTrigger("123"); }
            };

            string testLine = "...123";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 2);

            Assert.AreEqual(patternsInfo[0].Creator, null);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("..."));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 0);
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalEndPos, 2);

            Assert.AreEqual(patternsInfo[1].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[1]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[1]._PatternData[0].Content.Equals("123"));
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalStartPos, 3);
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalEndPos, 5);
        }

        [TestMethod]
        public void NoPatternSectorOnEnd()
        {
            PatternsSectorsSearcher searcher = new PatternsSectorsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new StringTrigger("123"); }
            };

            string testLine = "123...";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 2);

            Assert.AreEqual(patternsInfo[0].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("123"));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 0);
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalEndPos, 2);

            Assert.AreEqual(patternsInfo[1].Creator, null);
            Assert.AreEqual(patternsInfo[1]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[1]._PatternData[0].Content.Equals("..."));
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalStartPos, 3);
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalEndPos, 5);
        }

        [TestMethod]
        public void DiscardedRecursion()
        {
            PatternsSectorsSearcher searcher = new PatternsSectorsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Discarded_10to6(); },
                delegate { return new Discarded_5to3(); },
                delegate { return new Discarded_3to1(); }
            };

            string testLine = "aaaaaaaaaa";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 4);

            Assert.AreEqual(patternsInfo[0].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("aaaa"));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 0);
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalEndPos, 3);

            Assert.AreEqual(patternsInfo[1].Creator, creators[1]);
            Assert.AreEqual(patternsInfo[1]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[1]._PatternData[0].Content.Equals("aa"));
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalStartPos, 4);
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalEndPos, 5);

            Assert.AreEqual(patternsInfo[2].Creator, creators[2]);
            Assert.AreEqual(patternsInfo[2]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[2]._PatternData[0].Content.Equals("aa"));
            Assert.AreEqual(patternsInfo[2]._PatternData[0].LocalStartPos, 6);
            Assert.AreEqual(patternsInfo[2]._PatternData[0].LocalEndPos, 7);

            Assert.AreEqual(patternsInfo[3].Creator, null);
            Assert.AreEqual(patternsInfo[3]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[3]._PatternData[0].Content.Equals("aa"));
            Assert.AreEqual(patternsInfo[3]._PatternData[0].LocalStartPos, 8);
            Assert.AreEqual(patternsInfo[3]._PatternData[0].LocalEndPos, 9);
        }

        [TestMethod]
        public void ZeroLengthPatternOnEnd()
        {
            PatternsSectorsSearcher searcher = new PatternsSectorsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Branching().Add(new StringTrigger("123"))
                                                 .EnableSkip(); }
            };

            string testLine = "123.";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 2);

            Assert.AreEqual(patternsInfo[0].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("123"));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 0);
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalEndPos, 2);

            Assert.AreEqual(patternsInfo[1].Creator, null);
            Assert.AreEqual(patternsInfo[1]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[1]._PatternData[0].Content.Equals("."));
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalStartPos, 3);
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalEndPos, 3);
        }

        [TestMethod]
        public void ZeroLengthPatternOnStart()
        {
            PatternsSectorsSearcher searcher = new PatternsSectorsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Branching().Add(new StringTrigger("123"))
                                                 .EnableSkip(); }
            };

            string testLine = ".123";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 2);

            Assert.AreEqual(patternsInfo[0].Creator, null);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("."));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 0);
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalEndPos, 0);

            Assert.AreEqual(patternsInfo[1].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[1]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[1]._PatternData[0].Content.Equals("123"));
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalStartPos, 1);
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalEndPos, 3);
        }

        [TestMethod]
        public void ZeroLengthPatternBetweenPatterns()
        {
            PatternsSectorsSearcher searcher = new PatternsSectorsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Branching().Add(new StringTrigger("123"))
                                                 .EnableSkip(); }
            };

            string testLine = "123.123";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 3);

            Assert.AreEqual(patternsInfo[0].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("123"));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 0);
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalEndPos, 2);

            Assert.AreEqual(patternsInfo[1].Creator, null);
            Assert.AreEqual(patternsInfo[1]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[1]._PatternData[0].Content.Equals("."));
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalStartPos, 3);
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalEndPos, 3);

            Assert.AreEqual(patternsInfo[2].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[2]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[2]._PatternData[0].Content.Equals("123"));
            Assert.AreEqual(patternsInfo[2]._PatternData[0].LocalStartPos, 4);
            Assert.AreEqual(patternsInfo[2]._PatternData[0].LocalEndPos, 6);
        }

        [TestMethod]
        public void PatternsPriorities()
        {
            PatternsSectorsSearcher searcher = new PatternsSectorsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Sequence()
                                      .Add(new StringTrigger("1"))
                                      .Add(new StringTrigger("2"))
                                      .Add(new StringTrigger("3"))
                                      .Add(new StringTrigger("4"))
                                      .SetReleasedPatternNumber(1); },
                delegate { return new Sequence()
                                      .Add(new StringTrigger("1"))
                                      .Add(new StringTrigger("2"))
                                      .Add(new StringTrigger("3"))
                                      .Add(new StringTrigger("4"))
                                      .SetReleasedPatternNumber(2); },
                delegate { return new Sequence()
                                      .Add(new StringTrigger("1"))
                                      .Add(new StringTrigger("2"))
                                      .Add(new StringTrigger("3"))
                                      .Add(new StringTrigger("4"))
                                      .SetReleasedPatternNumber(3); }
            };

            string testLine = "1234";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 2);

            Assert.AreEqual(patternsInfo[0].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 3);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("1"));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 0);
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalEndPos, 0);
            Assert.IsTrue(patternsInfo[0]._PatternData[1].Content.Equals("2"));
            Assert.AreEqual(patternsInfo[0]._PatternData[1].LocalStartPos, 1);
            Assert.AreEqual(patternsInfo[0]._PatternData[1].LocalEndPos, 1);
            Assert.IsTrue(patternsInfo[0]._PatternData[2].Content.Equals("3"));
            Assert.AreEqual(patternsInfo[0]._PatternData[2].LocalStartPos, 2);
            Assert.AreEqual(patternsInfo[0]._PatternData[2].LocalEndPos, 2);

            Assert.AreEqual(patternsInfo[1].Creator, null);
            Assert.AreEqual(patternsInfo[1]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[1]._PatternData[0].Content.Equals("4"));
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalStartPos, 3);
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalEndPos, 3);
        }
    }
}
