﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Parcing.StringPatternsDetection;
using Parcing.StringPatternsDetection.Patterns;
using Parcing.StringPatternsDetection.PatternsSearchers;
using System;
using System.Collections.Generic;

namespace ParcingTests
{
    [TestClass]
    public class RecursionEntryTests
    {
        [TestMethod]
        public void Brackets_1()
        {
            IPattern RecursionPattern()
            {
                return new Repeat().Set(new Sequence().Add(new Symbol('('))
                                                      .Add(new RecursionEntry(RecursionPattern))
                                                      .Add(new Symbol(')')));
            }

            PatternsSearcher searcher = new PatternsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new RecursionEntry(RecursionPattern); }
            };

            string testLine = "()()()";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 1);

            Assert.AreEqual(patternsInfo[0].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 6);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("("));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 0);
            Assert.IsTrue(patternsInfo[0]._PatternData[1].Content.Equals(")"));
            Assert.AreEqual(patternsInfo[0]._PatternData[1].LocalStartPos, 1);
            Assert.IsTrue(patternsInfo[0]._PatternData[2].Content.Equals("("));
            Assert.AreEqual(patternsInfo[0]._PatternData[2].LocalStartPos, 2);
            Assert.IsTrue(patternsInfo[0]._PatternData[3].Content.Equals(")"));
            Assert.AreEqual(patternsInfo[0]._PatternData[3].LocalStartPos, 3);
            Assert.IsTrue(patternsInfo[0]._PatternData[4].Content.Equals("("));
            Assert.AreEqual(patternsInfo[0]._PatternData[4].LocalStartPos, 4);
            Assert.IsTrue(patternsInfo[0]._PatternData[5].Content.Equals(")"));
            Assert.AreEqual(patternsInfo[0]._PatternData[5].LocalStartPos, 5);
        }

        [TestMethod]
        public void Brackets_2()
        {
            IPattern RecursionPattern()
            {
                return new Repeat().Set(new Sequence().Add(new Symbol('('))
                                                      .Add(new RecursionEntry(RecursionPattern))
                                                      .Add(new Symbol(')')));
            }

            PatternsSearcher searcher = new PatternsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new RecursionEntry(RecursionPattern); }
            };

            string testLine = "(((()))())";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 1);

            Assert.AreEqual(patternsInfo[0].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 10);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("("));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 0);
            Assert.IsTrue(patternsInfo[0]._PatternData[1].Content.Equals("("));
            Assert.AreEqual(patternsInfo[0]._PatternData[1].LocalStartPos, 1);
            Assert.IsTrue(patternsInfo[0]._PatternData[2].Content.Equals("("));
            Assert.AreEqual(patternsInfo[0]._PatternData[2].LocalStartPos, 2);
            Assert.IsTrue(patternsInfo[0]._PatternData[3].Content.Equals("("));
            Assert.AreEqual(patternsInfo[0]._PatternData[3].LocalStartPos, 3);
            Assert.IsTrue(patternsInfo[0]._PatternData[4].Content.Equals(")"));
            Assert.AreEqual(patternsInfo[0]._PatternData[4].LocalStartPos, 4);
            Assert.IsTrue(patternsInfo[0]._PatternData[5].Content.Equals(")"));
            Assert.AreEqual(patternsInfo[0]._PatternData[5].LocalStartPos, 5);
            Assert.IsTrue(patternsInfo[0]._PatternData[6].Content.Equals(")"));
            Assert.AreEqual(patternsInfo[0]._PatternData[6].LocalStartPos, 6);
            Assert.IsTrue(patternsInfo[0]._PatternData[7].Content.Equals("("));
            Assert.AreEqual(patternsInfo[0]._PatternData[7].LocalStartPos, 7);
            Assert.IsTrue(patternsInfo[0]._PatternData[8].Content.Equals(")"));
            Assert.AreEqual(patternsInfo[0]._PatternData[8].LocalStartPos, 8);
            Assert.IsTrue(patternsInfo[0]._PatternData[9].Content.Equals(")"));
            Assert.AreEqual(patternsInfo[0]._PatternData[9].LocalStartPos, 9);
        }

        [TestMethod]
        public void ErrorsHandling()
        {
            IPattern RecursionPattern()
            {
                return new Repeat().Set(new Sequence().Add(new Symbol('('))
                                                      .Add(new Branching().Add(new RecursionEntry(RecursionPattern))
                                                                          .Add(new ErrorsTestPattern(1)))
                                                      .Add(new Symbol(')')));
            }

            PatternErrorInfo errorInfo = null;
            PatternsSearcher searcher = new PatternsSearcher();
            searcher.AddPatternsErrorsListener(error => errorInfo = error);

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new RecursionEntry(RecursionPattern); }
            };

            string testLine = "(()(aa))";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(errorInfo.Creator, creators[0]);
            Assert.AreEqual(errorInfo._PatternError.LocalStartPos, 5);
            Assert.AreEqual(errorInfo._PatternError.LocalEndPos, 5);
            Assert.AreEqual(errorInfo._PatternError.Length, 1);
            Assert.IsTrue(errorInfo._PatternError.Message.Equals("Detected excess 'a' symbol"));

            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack.Count, 9);
            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack[0], typeof(RecursionEntry));
            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack[1], typeof(Repeat));
            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack[2], typeof(Sequence));
            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack[3], typeof(Branching));
            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack[4], typeof(RecursionEntry));
            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack[5], typeof(Repeat));
            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack[6], typeof(Sequence));
            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack[7], typeof(Branching));
            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack[8], typeof(ErrorsTestPattern));
        }
    }
}
