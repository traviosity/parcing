﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Parcing.StringPatternsDetection;
using Parcing.StringPatternsDetection.Patterns;
using Parcing.StringPatternsDetection.PatternsSearchers;
using System;
using System.Collections.Generic;

namespace ParcingTests
{
    [TestClass]
    public class RepeatTests
    {
        [TestMethod]
        public void Symbols()
        {
            PatternsSearcher searcher = new PatternsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Repeat().Set(new Symbol('a')); }
            };

            string testLine = "aaa";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 1);

            Assert.AreEqual(patternsInfo[0].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 3);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("a"));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 0);
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalEndPos, 0);
            Assert.IsTrue(patternsInfo[0]._PatternData[1].Content.Equals("a"));
            Assert.AreEqual(patternsInfo[0]._PatternData[1].LocalStartPos, 1);
            Assert.AreEqual(patternsInfo[0]._PatternData[1].LocalEndPos, 1);
            Assert.IsTrue(patternsInfo[0]._PatternData[2].Content.Equals("a"));
            Assert.AreEqual(patternsInfo[0]._PatternData[2].LocalStartPos, 2);
            Assert.AreEqual(patternsInfo[0]._PatternData[2].LocalEndPos, 2);
        }

        [TestMethod]
        public void DiscardedRecursion_1()
        {
            PatternsSearcher searcher = new PatternsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Repeat().Set(new Discarded_Repeat()); }
            };

            string testLine = "abababg";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 1);

            Assert.AreEqual(patternsInfo[0].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 3);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("ab"));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 0);
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalEndPos, 1);
            Assert.IsTrue(patternsInfo[0]._PatternData[1].Content.Equals("ab"));
            Assert.AreEqual(patternsInfo[0]._PatternData[1].LocalStartPos, 2);
            Assert.AreEqual(patternsInfo[0]._PatternData[1].LocalEndPos, 3);
            Assert.IsTrue(patternsInfo[0]._PatternData[2].Content.Equals("ab"));
            Assert.AreEqual(patternsInfo[0]._PatternData[2].LocalStartPos, 4);
            Assert.AreEqual(patternsInfo[0]._PatternData[2].LocalEndPos, 5);
        }

        [TestMethod]
        public void DiscardedRecursion_2()
        {
            PatternsSearcher searcher = new PatternsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Repeat().Set(new Discarded_Repeat()); }
            };

            string testLine = "ababab";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 1);

            Assert.AreEqual(patternsInfo[0].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 3);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("ab"));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 0);
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalEndPos, 1);
            Assert.IsTrue(patternsInfo[0]._PatternData[1].Content.Equals("ab"));
            Assert.AreEqual(patternsInfo[0]._PatternData[1].LocalStartPos, 2);
            Assert.AreEqual(patternsInfo[0]._PatternData[1].LocalEndPos, 3);
            Assert.IsTrue(patternsInfo[0]._PatternData[2].Content.Equals("ab"));
            Assert.AreEqual(patternsInfo[0]._PatternData[2].LocalStartPos, 4);
            Assert.AreEqual(patternsInfo[0]._PatternData[2].LocalEndPos, 5);
        }

        [TestMethod]
        public void ExactlyRepeat_1()
        {
            PatternsSearcher searcher = new PatternsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Repeat().Set(new StringTrigger("ab"))
                                              .SetMinMax(3); }
            };

            string testLine = "ababab";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 1);

            Assert.AreEqual(patternsInfo[0].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 3);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("ab"));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 0);
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalEndPos, 1);
            Assert.IsTrue(patternsInfo[0]._PatternData[1].Content.Equals("ab"));
            Assert.AreEqual(patternsInfo[0]._PatternData[1].LocalStartPos, 2);
            Assert.AreEqual(patternsInfo[0]._PatternData[1].LocalEndPos, 3);
            Assert.IsTrue(patternsInfo[0]._PatternData[2].Content.Equals("ab"));
            Assert.AreEqual(patternsInfo[0]._PatternData[2].LocalStartPos, 4);
            Assert.AreEqual(patternsInfo[0]._PatternData[2].LocalEndPos, 5);
        }

        [TestMethod]
        public void ExactlyRepeat_2()
        {
            PatternsSearcher searcher = new PatternsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Repeat().Set(new StringTrigger("ab"))
                                              .SetMinMax(3); }
            };

            string testLine = "abababg";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 1);

            Assert.AreEqual(patternsInfo[0].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 3);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("ab"));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 0);
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalEndPos, 1);
            Assert.IsTrue(patternsInfo[0]._PatternData[1].Content.Equals("ab"));
            Assert.AreEqual(patternsInfo[0]._PatternData[1].LocalStartPos, 2);
            Assert.AreEqual(patternsInfo[0]._PatternData[1].LocalEndPos, 3);
            Assert.IsTrue(patternsInfo[0]._PatternData[2].Content.Equals("ab"));
            Assert.AreEqual(patternsInfo[0]._PatternData[2].LocalStartPos, 4);
            Assert.AreEqual(patternsInfo[0]._PatternData[2].LocalEndPos, 5);
        }

        [TestMethod]
        public void ExactlyRepeat_3()
        {
            PatternsSearcher searcher = new PatternsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Repeat().Set(new StringTrigger("ab"))
                                              .SetMinMax(2); }
            };

            string testLine = "ababab";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 1);

            Assert.AreEqual(patternsInfo[0].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 2);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("ab"));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 0);
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalEndPos, 1);
            Assert.IsTrue(patternsInfo[0]._PatternData[1].Content.Equals("ab"));
            Assert.AreEqual(patternsInfo[0]._PatternData[1].LocalStartPos, 2);
            Assert.AreEqual(patternsInfo[0]._PatternData[1].LocalEndPos, 3);
        }

        [TestMethod]
        public void ExactlyRepeat_4()
        {
            PatternsSearcher searcher = new PatternsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Repeat().Set(new StringTrigger("ab"))
                                              .SetMin(4)
                                              .SetMax(4); }
            };

            string testLine = "ababab";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 0);
        }

        [TestMethod]
        public void ExactlyRepeat_5()
        {
            PatternsSearcher searcher = new PatternsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Repeat().Set(new StringTrigger("ab"))
                                              .SetMax(4)
                                              .SetMin(4); }
            };

            string testLine = "abababgf";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 0);
        }

        [TestMethod]
        public void ErrorsHandling()
        {
            PatternErrorInfo errorInfo = null;
            PatternsSearcher searcher = new PatternsSearcher();
            searcher.AddPatternsErrorsListener(error => errorInfo = error);

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Repeat().Set(new ErrorsTestPattern(3)); }
            };

            string testLine = "ccabaaaa";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(errorInfo.Creator, creators[0]);
            Assert.AreEqual(errorInfo._PatternError.LocalStartPos, 7);
            Assert.AreEqual(errorInfo._PatternError.LocalEndPos, 7);
            Assert.AreEqual(errorInfo._PatternError.Length, 1);
            Assert.IsTrue(errorInfo._PatternError.Message.Equals("Detected excess 'a' symbol"));

            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack.Count, 2);
            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack[0], typeof(Repeat));
            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack[1], typeof(ErrorsTestPattern));
        }

        [TestMethod]
        public void AbortErrorsHandling_1()
        {
            PatternErrorInfo errorInfo = null;
            PatternsSearcher searcher = new PatternsSearcher();
            searcher.AddPatternsErrorsListener(error => errorInfo = error);

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Repeat().Set(new Discarded_Repeat()).SetMin(3)
                                                                          .EnableAbortError(data => "My error"); }
            };

            string testLine = "ababag";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 0);

            Assert.AreEqual(errorInfo.Creator, creators[0]);
            Assert.AreEqual(errorInfo._PatternError.LocalStartPos, 4);
            Assert.AreEqual(errorInfo._PatternError.LocalEndPos, 4);
            Assert.AreEqual(errorInfo._PatternError.Length, 1);
            Assert.IsTrue(errorInfo._PatternError.Message.Equals("My error"));

            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack.Count, 1);
            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack[0], typeof(Repeat));
        }

        [TestMethod]
        public void AbortErrorsHandling_2()
        {
            PatternErrorInfo errorInfo = null;
            PatternsSearcher searcher = new PatternsSearcher();
            searcher.AddPatternsErrorsListener(error => errorInfo = error);

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Repeat().Set(new Discarded_Repeat()).SetMin(3)
                                                                          .EnableAbortError(data => "My error"); }
            };

            string testLine = "abab";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 0);

            Assert.AreEqual(errorInfo.Creator, creators[0]);
            Assert.AreEqual(errorInfo._PatternError.LocalStartPos, 3);
            Assert.AreEqual(errorInfo._PatternError.LocalEndPos, 3);
            Assert.AreEqual(errorInfo._PatternError.Length, 1);
            Assert.IsTrue(errorInfo._PatternError.Message.Equals("My error"));

            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack.Count, 1);
            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack[0], typeof(Repeat));
        }

        [TestMethod]
        public void AbortErrorsHandling_3()
        {
            PatternErrorInfo errorInfo = null;
            PatternsSearcher searcher = new PatternsSearcher();
            searcher.AddPatternsErrorsListener(error => errorInfo = error);

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Repeat().Set(new Discarded_Repeat()).SetMin(3)
                                                                          .EnableAbortError(data => "My error", data => 0, data => data.Count * 2); }
            };

            string testLine = "ababag";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 0);

            Assert.AreEqual(errorInfo.Creator, creators[0]);
            Assert.AreEqual(errorInfo._PatternError.LocalStartPos, 0);
            Assert.AreEqual(errorInfo._PatternError.LocalEndPos, 3);
            Assert.AreEqual(errorInfo._PatternError.Length, 4);
            Assert.IsTrue(errorInfo._PatternError.Message.Equals("My error"));

            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack.Count, 1);
            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack[0], typeof(Repeat));
        }
    }
}
