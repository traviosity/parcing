﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Parcing.StringPatternsDetection;
using Parcing.StringPatternsDetection.Patterns;
using Parcing.StringPatternsDetection.PatternsSearchers;
using System;
using System.Collections.Generic;

namespace ParcingTests
{
    [TestClass]
    public class SequenceTests
    {
        [TestMethod]
        public void Symbols()
        {
            PatternsSearcher searcher = new PatternsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Sequence().Add(new Symbol('a'))
                                                .Add(new Symbol('b'))
                                                .Add(new Symbol('c')); }
            };

            string testLine = "abc";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 1);

            Assert.AreEqual(patternsInfo[0].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 3);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("a"));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 0);
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalEndPos, 0);
            Assert.IsTrue(patternsInfo[0]._PatternData[1].Content.Equals("b"));
            Assert.AreEqual(patternsInfo[0]._PatternData[1].LocalStartPos, 1);
            Assert.AreEqual(patternsInfo[0]._PatternData[1].LocalEndPos, 1);
            Assert.IsTrue(patternsInfo[0]._PatternData[2].Content.Equals("c"));
            Assert.AreEqual(patternsInfo[0]._PatternData[2].LocalStartPos, 2);
            Assert.AreEqual(patternsInfo[0]._PatternData[2].LocalEndPos, 2);
        }

        [TestMethod]
        public void ErrorsHandling()
        {
            PatternErrorInfo errorInfo = null;
            PatternsSearcher searcher = new PatternsSearcher();
            searcher.AddPatternsErrorsListener(error => errorInfo = error);

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Sequence().Add(new Symbol('a'))
                                                .Add(new Symbol('b'))
                                                .Add(new ErrorsTestPattern(3)); }
            };

            string testLine = "ccabaaaa";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(errorInfo.Creator, creators[0]);
            Assert.AreEqual(errorInfo._PatternError.LocalStartPos, 7);
            Assert.AreEqual(errorInfo._PatternError.LocalEndPos, 7);
            Assert.AreEqual(errorInfo._PatternError.Length, 1);
            Assert.IsTrue(errorInfo._PatternError.Message.Equals("Detected excess 'a' symbol"));

            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack.Count, 2);
            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack[0], typeof(Sequence));
            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack[1], typeof(ErrorsTestPattern));
        }

        [TestMethod]
        public void AbortErrorsHandling_1()
        {
            PatternErrorInfo errorInfo = null;
            PatternsSearcher searcher = new PatternsSearcher();
            searcher.AddPatternsErrorsListener(error => errorInfo = error);

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Sequence().Add(new Symbol('a'))
                                                .Add(new Symbol('b'))
                                                .Add(new Symbol('c'))
                                                .AddAbortError(2, data => "My error"); }
            };

            string testLine = "aba";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 0);

            Assert.AreEqual(errorInfo.Creator, creators[0]);
            Assert.AreEqual(errorInfo._PatternError.LocalStartPos, 2);
            Assert.AreEqual(errorInfo._PatternError.LocalEndPos, 2);
            Assert.AreEqual(errorInfo._PatternError.Length, 1);
            Assert.IsTrue(errorInfo._PatternError.Message.Equals("My error"));

            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack.Count, 1);
            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack[0], typeof(Sequence));
        }

        [TestMethod]
        public void AbortErrorsHandling_2()
        {
            PatternErrorInfo errorInfo = null;
            PatternsSearcher searcher = new PatternsSearcher();
            searcher.AddPatternsErrorsListener(error => errorInfo = error);

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Sequence().Add(new Symbol('a'))
                                                .Add(new Symbol('b'))
                                                .Add(new Symbol('c'))
                                                .AddAbortError(2, data => "My error"); }
            };

            string testLine = "ab";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 0);

            Assert.AreEqual(errorInfo.Creator, creators[0]);
            Assert.AreEqual(errorInfo._PatternError.LocalStartPos, 1);
            Assert.AreEqual(errorInfo._PatternError.LocalEndPos, 1);
            Assert.AreEqual(errorInfo._PatternError.Length, 1);
            Assert.IsTrue(errorInfo._PatternError.Message.Equals("My error"));

            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack.Count, 1);
            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack[0], typeof(Sequence));
        }

        [TestMethod]
        public void AbortErrorsHandling_3()
        {
            PatternErrorInfo errorInfo = null;
            PatternsSearcher searcher = new PatternsSearcher();
            searcher.AddPatternsErrorsListener(error => errorInfo = error);

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Sequence().Add(new Symbol('a'))
                                                .Add(new Symbol('b'))
                                                .Add(new Symbol('c'))
                                                .AddAbortError(2, data => "My error", data => 0, data => data.Count + 1); }
            };

            string testLine = "aba";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 0);

            Assert.AreEqual(errorInfo.Creator, creators[0]);
            Assert.AreEqual(errorInfo._PatternError.LocalStartPos, 0);
            Assert.AreEqual(errorInfo._PatternError.LocalEndPos, 2);
            Assert.AreEqual(errorInfo._PatternError.Length, 3);
            Assert.IsTrue(errorInfo._PatternError.Message.Equals("My error"));

            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack.Count, 1);
            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack[0], typeof(Sequence));
        }

        [TestMethod]
        public void AbortErrorsHandling_4()
        {
            PatternErrorInfo errorInfo = null;
            PatternsSearcher searcher = new PatternsSearcher();
            searcher.AddPatternsErrorsListener(error => errorInfo = error);

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Sequence().Add(new Discarded_Repeat())
                                                .Add(new Discarded_Repeat())
                                                .Add(new Discarded_Repeat())
                                                .AddAbortError(2, data => "My error"); }
            };

            string testLine = "ababag";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 0);

            Assert.AreEqual(errorInfo.Creator, creators[0]);
            Assert.AreEqual(errorInfo._PatternError.LocalStartPos, 4);
            Assert.AreEqual(errorInfo._PatternError.LocalEndPos, 4);
            Assert.AreEqual(errorInfo._PatternError.Length, 1);
            Assert.IsTrue(errorInfo._PatternError.Message.Equals("My error"));

            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack.Count, 1);
            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack[0], typeof(Sequence));
        }

        [TestMethod]
        public void AbortErrorsHandling_5()
        {
            PatternErrorInfo errorInfo = null;
            PatternsSearcher searcher = new PatternsSearcher();
            searcher.AddPatternsErrorsListener(error => errorInfo = error);

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Sequence().Add(new Discarded_Repeat())
                                                .Add(new Discarded_Repeat())
                                                .Add(new Discarded_Repeat())
                                                .AddAbortError(2, data => "My error"); }
            };

            string testLine = "abab";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 0);

            Assert.AreEqual(errorInfo.Creator, creators[0]);
            Assert.AreEqual(errorInfo._PatternError.LocalStartPos, 3);
            Assert.AreEqual(errorInfo._PatternError.LocalEndPos, 3);
            Assert.AreEqual(errorInfo._PatternError.Length, 1);
            Assert.IsTrue(errorInfo._PatternError.Message.Equals("My error"));

            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack.Count, 1);
            Assert.AreEqual(errorInfo._PatternError.PatternsCallStack[0], typeof(Sequence));
        }

        [TestMethod]
        public void DiscardedRecursion_1()
        {
            PatternsSearcher searcher = new PatternsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Sequence().Add(new Discarded_10to6())
                                                .Add(new Discarded_5to3())
                                                .Add(new Discarded_3to1()); }
            };

            string testLine = "baaaaaaaaaa";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 1);

            Assert.AreEqual(patternsInfo[0].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 3);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("aaaa"));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 1);
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalEndPos, 4);
            Assert.IsTrue(patternsInfo[0]._PatternData[1].Content.Equals("aa"));
            Assert.AreEqual(patternsInfo[0]._PatternData[1].LocalStartPos, 5);
            Assert.AreEqual(patternsInfo[0]._PatternData[1].LocalEndPos, 6);
            Assert.IsTrue(patternsInfo[0]._PatternData[2].Content.Equals("aa"));
            Assert.AreEqual(patternsInfo[0]._PatternData[2].LocalStartPos, 7);
            Assert.AreEqual(patternsInfo[0]._PatternData[2].LocalEndPos, 8);
        }

        [TestMethod]
        public void DiscardedRecursion_2()
        {
            PatternsSearcher searcher = new PatternsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Sequence().Add(new Discarded_Repeat())
                                                .Add(new Discarded_Repeat())
                                                .Add(new Discarded_Repeat()); }
            };

            string testLine = "ababab";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 1);

            Assert.AreEqual(patternsInfo[0].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 3);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("ab"));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 0);
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalEndPos, 1);
            Assert.IsTrue(patternsInfo[0]._PatternData[1].Content.Equals("ab"));
            Assert.AreEqual(patternsInfo[0]._PatternData[1].LocalStartPos, 2);
            Assert.AreEqual(patternsInfo[0]._PatternData[1].LocalEndPos, 3);
            Assert.IsTrue(patternsInfo[0]._PatternData[2].Content.Equals("ab"));
            Assert.AreEqual(patternsInfo[0]._PatternData[2].LocalStartPos, 4);
            Assert.AreEqual(patternsInfo[0]._PatternData[2].LocalEndPos, 5);
        }

        [TestMethod]
        public void ReleasedPatterns_1()
        {
            PatternsSectorsSearcher searcher = new PatternsSectorsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Sequence().Add(new StringTrigger("ab"))
                                                .Add(new StringTrigger("ab"))
                                                .Add(new StringTrigger("ab"))
                                                .SetReleasedPatternNumber(1); },

                delegate { return new StringTrigger("ab"); }
            };

            string testLine = "ababab";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 2);

            Assert.AreEqual(patternsInfo[0].Creator, creators[0]);
            Assert.AreEqual(patternsInfo[0]._PatternData.Count, 2);
            Assert.IsTrue(patternsInfo[0]._PatternData[0].Content.Equals("ab"));
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalStartPos, 0);
            Assert.AreEqual(patternsInfo[0]._PatternData[0].LocalEndPos, 1);
            Assert.IsTrue(patternsInfo[0]._PatternData[1].Content.Equals("ab"));
            Assert.AreEqual(patternsInfo[0]._PatternData[1].LocalStartPos, 2);
            Assert.AreEqual(patternsInfo[0]._PatternData[1].LocalEndPos, 3);

            Assert.AreEqual(patternsInfo[1].Creator, creators[1]);
            Assert.AreEqual(patternsInfo[1]._PatternData.Count, 1);
            Assert.IsTrue(patternsInfo[1]._PatternData[0].Content.Equals("ab"));
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalStartPos, 4);
            Assert.AreEqual(patternsInfo[1]._PatternData[0].LocalEndPos, 5);
        }

        [TestMethod]
        public void ReleasedPatterns_2()
        {
            PatternsSearcher searcher = new PatternsSearcher();

            List<Func<IPattern>> creators = new List<Func<IPattern>>
            {
                delegate { return new Sequence().Add(new StringTrigger("ab"))
                                                .Add(new StringTrigger("ab"))
                                                .Add(new StringTrigger("ab"))
                                                .SetReleasedPatternNumber(3); }
            };

            string testLine = "ababab";

            var patternsInfo = TestsHelpers.DetectPatterns(searcher, creators, testLine);

            Assert.AreEqual(patternsInfo.Count, 1);
        }
    }
}
