﻿using Parcing.StringPatternsDetection;
using System;
using System.Collections.Generic;

namespace ParcingTests
{
    public static class TestsHelpers
    {
        public static List<PatternDataInfo> DetectPatterns(IPatternsSearcher searcher, List<Func<IPattern>> creators, string testLine)
        {
            for (int i = 0; i < creators.Count; i++)
                searcher.AddPattern(creators[i]);

            for (int i = 0; i < testLine.Length; i++)
                searcher.CheckNextSymbol(testLine[i]);
            searcher.OnSymbolsEnd();

            List<PatternDataInfo> patternsInfo = new List<PatternDataInfo>();
            while (searcher.PatternsDetected)
                patternsInfo.Add(searcher.PullNextDetectedPattern());

            return patternsInfo;
        }
    }
}
